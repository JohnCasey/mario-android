package com.mojang.mario;


import java.io.DataInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import nz.co.unitec.main.R;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

import com.mojang.mario.level.BgLevelGenerator;
import com.mojang.mario.level.Level;
import com.mojang.mario.level.LevelGenerator;
import com.mojang.mario.level.SpriteTemplate;
import com.mojang.mario.sprites.BulletBill;
import com.mojang.mario.sprites.CoinAnim;
import com.mojang.mario.sprites.FireFlower;
import com.mojang.mario.sprites.Fireball;
import com.mojang.mario.sprites.Mario;
import com.mojang.mario.sprites.Mushroom;
import com.mojang.mario.sprites.Particle;
import com.mojang.mario.sprites.Shell;
import com.mojang.mario.sprites.Sparkle;
import com.mojang.mario.sprites.Sprite;
import com.mojang.mario.sprites.SpriteContext;
import com.mojang.sonar.AndroidSoundEngine;


public class LevelScene extends Scene implements SpriteContext {
	private List<Sprite> sprites = new ArrayList<Sprite>();
	private List<Sprite> spritesToAdd = new ArrayList<Sprite>();
	private List<Sprite> spritesToRemove = new ArrayList<Sprite>();

	public Level level;
	public Mario mario;
	public float xCam, yCam, xCamO, yCamO;
	public static Bitmap tmpImage;
	private int tick;

	private LevelRenderer layer;
	private BgRenderer[] bgLayer = new BgRenderer[2];

	//private GraphicsConfiguration graphicsConfiguration;

	public boolean paused = false;
	public int startTime = 0;
	private int timeLeft;

	// private Recorder recorder = new Recorder();
	// private Replayer replayer = null;

	private long levelSeed;
	private MarioComponent renderer;
	private int levelType;
	private int levelDifficulty;

	float idleBloodSugarDrop = 0.005f;
	float walkingBloodSugarDrop = 0.02f;
	float runningBloodSugarDrop = 0.06f;
	float jumpBloodSugarDrop = 0.09f;
	float insDrop = 5;

	float maxBloodSugarSafe = 7f;
	float minBloodSugarSafe;

	float maxBloodSugarWarning;
	float minBloodSugarWarning;

	float maxBloodSugarCritical= 15f;
	float minBloodSugarCritical;

	int CriticalBloodSugarColor;
	int WarningBloodSugarColor;
	int SafeBloodSugarColor;
	float feedbackLevel = 3.0f;
	boolean jumped = false;
	int idleTime = 0;
	int walkingTime = 0;
	int runningTime = 0;
	boolean usedFood = false;
	boolean usedIns = false;
	boolean gameModeDibetic = true;
	public boolean drawNormalFoodMenu = false;
	public boolean drawMainMenu = false;
	public boolean drawUrgentFoodMenu = false;
	public boolean showenMenu = false;
	public boolean drawFeedback = false;
	boolean showenFeedback = false;

	boolean bloodSugarWarning = false;
	boolean bloodSugarCritical = false;

	String feedbackMessageLine1 = "";
	String feedbackMessageLine2 = "";
	String feedbackMessageLine3 = "";

	public LevelScene(
			MarioComponent renderer, long seed, int levelDifficulty, int type)
	{
		this.levelSeed = seed;
		this.renderer = renderer;
		this.levelDifficulty = levelDifficulty;
		this.levelType = type;
	}

	public void init() {
		try {
			Level.loadBehaviors(new DataInputStream(LevelScene.class
					.getResourceAsStream("tiles.dat")));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		/*
		 * if (replayer!=null) { level = LevelGenerator.createLevel(2048, 15,
		 * replayer.nextLong()); } else {
		 */
		// level = LevelGenerator.createLevel(320, 15, levelSeed);
		level = LevelGenerator.createLevel(320, 15, levelSeed, levelDifficulty,
				levelType);
		// }

		/*
		 * if (recorder != null) { recorder.addLong(LevelGenerator.lastSeed); }
		 */

		if (levelType == LevelGenerator.TYPE_OVERGROUND)
			Art.startMusic(R.raw.smb3ovr1);
		else if (levelType == LevelGenerator.TYPE_UNDERGROUND)
			Art.startMusic(R.raw.smb3undr);
		else if (levelType == LevelGenerator.TYPE_CASTLE)
			Art.startMusic(R.raw.smwfortress);

		paused = false;
		Sprite.spriteContext = this;
		sprites.clear();
		layer = new LevelRenderer(level, 320, 240);//graphicsConfiguration
		for (int i = 0; i < 2; i++) {
			int scrollSpeed = 4 >> i;
			int w = ((level.width * 16) - 320) / scrollSpeed + 320;
			int h = ((level.height * 16) - 240) / scrollSpeed + 240;
			Level bgLevel = BgLevelGenerator.createLevel(w / 32 + 1,
					h / 32 + 1, i == 0, levelType);
			bgLayer[i] = new BgRenderer(bgLevel, 320,
					240, scrollSpeed);//graphicsConfiguration
		}
		mario = new Mario(this);
		sprites.add(mario);
		startTime = 1;

		timeLeft = 800 * 15;

		tick = 0;

		loadData();
		if (Integer.parseInt(INIManager.ReadValue("gameMode")) == 1)
			gameModeDibetic = true;
		else
			gameModeDibetic = false;
		
		renderer.getActivity().runOnUiThread(new Runnable(){
    		public void run(){
		    	for(View control: renderer.getControls())
		    	{
		    		control.setVisibility(View.VISIBLE);
					control.setEnabled(true);
		    	}
    		}
    	});
	}

	public int fireballsOnScreen = 0;

	List<Shell> shellsToCheck = new ArrayList<Shell>();

	public void checkShellCollide(Shell shell) {
		shellsToCheck.add(shell);
	}

	List<Fireball> fireballsToCheck = new ArrayList<Fireball>();

	public void checkFireballCollide(Fireball fireball) {
		fireballsToCheck.add(fireball);
	}

	public void tick() {
		if (!paused)
			timeLeft--;

		if (Mario.displayedHelp == false) {

			UpdateHelp();
		}
		if (timeLeft == 0) {
			mario.die();
		}
		xCamO = xCam;
		yCamO = yCam;

		if (startTime > 0) {
			startTime++;
		}

		float targetXCam = mario.x - 160;

		xCam = targetXCam;

		if (xCam < 0)
			xCam = 0;
		if (xCam > level.width * 16 - 320)
			xCam = level.width * 16 - 320;

		/*
		 * if (recorder != null) { recorder.addTick(mario.getKeyMask()); }
		 * 
		 * if (replayer!=null) { mario.setKeys(replayer.nextTick()); }
		 */

		fireballsOnScreen = 0;

		for (Sprite sprite : sprites) {
			if (sprite != mario) {
				float xd = sprite.x - xCam;
				float yd = sprite.y - yCam;
				if (xd < -64 || xd > 320 + 64 || yd < -64 || yd > 240 + 64) {
					removeSprite(sprite);
				} else {
					if (sprite instanceof Fireball) {
						fireballsOnScreen++;
					}
				}
			}
		}

		if (paused) {
			for (Sprite sprite : sprites) {
				if (sprite == mario) {
					sprite.tick();
				} else {
					sprite.tickNoMove();
				}
			}
		} else {
			tick++;
			level.tick();

			boolean hasShotCannon = false;
			int xCannon = 0;

			for (int x = (int) xCam / 16 - 1; x <= (int) (xCam + layer.width) / 16 + 1; x++)
				for (int y = (int) yCam / 16 - 1; y <= (int) (yCam + layer.height) / 16 + 1; y++) {
					int dir = 0;

					if (x * 16 + 8 > mario.x + 16)
						dir = -1;
					if (x * 16 + 8 < mario.x - 16)
						dir = 1;

					SpriteTemplate st = level.getSpriteTemplate(x, y);

					if (st != null) {
						if (st.lastVisibleTick != tick - 1) {
							if (st.sprite == null
									|| !sprites.contains(st.sprite)) {
								st.spawn(this, x, y, dir);
							}
						}

						st.lastVisibleTick = tick;
					}

					if (dir != 0) {
						byte b = level.getBlock(x, y);
						if (((Level.TILE_BEHAVIORS[b & 0xff]) & Level.BIT_ANIMATED) > 0) {
							if ((b % 16) / 4 == 3 && b / 16 == 0) {
								if ((tick - x * 2) % 100 == 0) {
									xCannon = x;
									for (int i = 0; i < 8; i++) {
										addSprite(new Sparkle(x * 16 + 8, y
												* 16
												+ (int) (Math.random() * 16),
												(float) Math.random() * dir, 0,
												0, 1, 5));
									}
									addSprite(new BulletBill(this, x * 16 + 8
											+ dir * 8, y * 16 + 15, dir));
									hasShotCannon = true;
								}
							}
						}
					}
				}

			if (hasShotCannon) {
				AndroidSoundEngine.play(R.raw.cannon);
				//sound.play(Art.samples[Art.SAMPLE_CANNON_FIRE],
				//		new FixedSoundSource(xCannon * 16, yCam + 120), 1, 1, 1);
			}

			for (Sprite sprite : sprites) {
				sprite.tick();
			}

			for (Sprite sprite : sprites) {
				sprite.collideCheck();
			}

			for (Shell shell : shellsToCheck) { 
				for (Sprite sprite : sprites) {
					if (sprite != shell && !shell.dead) {
						if (sprite.shellCollideCheck(shell)) {
							if (mario.carried == shell && !shell.dead) {
								mario.carried = null;
								shell.die();
							}
						}
					}
				}
			}
			shellsToCheck.clear();

			for (Fireball fireball : fireballsToCheck) {
				for (Sprite sprite : sprites) {
					if (sprite != fireball && !fireball.dead) {
						if (sprite.fireballCollideCheck(fireball)) {
							fireball.die();
						}
					}
				}
			}
			fireballsToCheck.clear();
		}
		if (gameModeDibetic) {
			if (!paused) {
				if (!jumped) {
					if (!mario.onGround) {
						Mario.bloodSugar -= jumpBloodSugarDrop;
						jumped = true;
					}
				}
				if (mario.onGround) {
					jumped = false;

					if (mario.isWalking) {
						walkingTime++;
					} else if (mario.isRunning)
						runningTime++;
					else
						idleTime++;
				}
				if (walkingTime > 60) {
					walkingTime = 0;
					Mario.bloodSugar -= walkingBloodSugarDrop;
				}
				if (idleTime > 60) {
					idleTime = 0;
					Mario.bloodSugar -= idleBloodSugarDrop;
				}
				if (runningTime > 30) {
					runningTime = 0;
					Mario.bloodSugar -= runningBloodSugarDrop;
				}
			}
			/*
			 * if (keys[Mario.KEY_INS]) { if (!usedIns) { bloodSugar -= insDrop;
			 * } usedIns = true; } else usedIns = false;
			 */
			if (keys[Mario.KEY_MENU]) {
				if (!usedFood)
					if (!drawNormalFoodMenu)
						if (!drawUrgentFoodMenu) {
							CSVLogger.LogUserSelectedMenu(name, sessionNum,
									Mario.bloodSugar, "Main Menu");
							drawMainMenu = !drawMainMenu;
							paused = drawMainMenu;
							usedFood = true;
						}
			} else
				usedFood = false;
			if (drawMainMenu)
				updateMainMenu();

			if (drawUrgentFoodMenu)
				updateUrgentFoodMenu();

			if (drawNormalFoodMenu) {
				updateNormalFoodMenu();
			}
			/*
			 * if (Mario.bloodSugar < minBloodSugarCritical) if
			 * (!drawNormalFoodMenu) if (!showenMenu){
			 * CSVLogger.LogForceMenu(name, sessionNum); drawNormalFoodMenu =
			 * true; paused = true; showenMenu = true; }
			 */
			// else
			if (Mario.bloodSugar > maxBloodSugarCritical) {
				Mario.bloodSugar = maxBloodSugarCritical;
			}

			if (Mario.bloodSugar < feedbackLevel)
				if (!showenFeedback) {
					paused = true;
					drawFeedback = true;
					showenFeedback = true;
					CSVLogger.LogFeedbackGiven(name, sessionNum,
							Mario.bloodSugar, feedbackMessageLine1 + " "
									+ feedbackMessageLine2 + " "
									+ feedbackMessageLine3);
				}

			if (Between(Mario.bloodSugar, minBloodSugarWarning,
					maxBloodSugarWarning)) {
				if (!bloodSugarWarning) {
					bloodSugarWarning = true;
					CSVLogger.LogWarning(name, sessionNum, Mario.bloodSugar);
				}
			} else
				bloodSugarWarning = false;

			if (Between(Mario.bloodSugar, minBloodSugarCritical,
					maxBloodSugarCritical)) {
				if (!bloodSugarCritical) {
					bloodSugarCritical = true;
					CSVLogger.LogCritical(name, sessionNum, Mario.bloodSugar);
				}
			} else
				bloodSugarCritical = false;
			/*
			 * if (!drawUrgentFoodMenu) if (!showenMenu){
			 * CSVLogger.LogForceMenu(name, sessionNum); drawUrgentFoodMenu =
			 * true; paused = true; showenMenu = true; }
			 * 
			 * else showenMenu = false;
			 */
		}
		sprites.addAll(0, spritesToAdd);
		sprites.removeAll(spritesToRemove);
		spritesToAdd.clear();
		spritesToRemove.clear();
	}

	private DecimalFormat df3 = new DecimalFormat("0");

	private DecimalFormat df = new DecimalFormat("00");
	private DecimalFormat df2 = new DecimalFormat("000");

	public void render(Canvas g, float alpha, Paint paint) {
		//TODO This
		int xCam = (int) (mario.xOld + (mario.x - mario.xOld) * alpha) - 160;
		int yCam = (int) (mario.yOld + (mario.y - mario.yOld) * alpha) - 120;
		// int xCam = (int) (xCamO + (this.xCam - xCamO) * alpha);
		// int yCam = (int) (yCamO + (this.yCam - yCamO) * alpha);
		if (xCam < 0)
			xCam = 0;
		if (yCam < 0)
			yCam = 0;
		if (xCam > level.width * 16 - 320)
			xCam = level.width * 16 - 320;
		if (yCam > level.height * 16 - 240)
			yCam = level.height * 16 - 240;

		// g.drawImage(Art.background, 0, 0, null);

		for (int i = 0; i < 2; i++) {
			bgLayer[i].setCam(xCam, yCam);
			bgLayer[i].render(g, tick, alpha, paint);
		}

		g.translate(-xCam, -yCam);
		
		
		//these are coins ect
		for (Sprite sprite : sprites) {
			if (sprite.layer == 0)
				sprite.render(g, alpha);
		}
		g.translate(xCam, yCam);
		
		layer.setCam(xCam, yCam);
		layer.render(g, tick, paused ? 0 : alpha);
		layer.renderExit0(g, tick, paused ? 0 : alpha, mario.winTime == 0);

		g.translate(-xCam, -yCam);
		//mario???
		for (Sprite sprite : sprites) {
			if (sprite.layer == 1)
				sprite.render(g, alpha);
		}
		//mario.render(g, alpha);
		g.translate(xCam, yCam);
		//g.drawColor(Color.BLACK);
		layer.renderExit1(g, tick, paused ? 0 : alpha);

		
		drawStringDropShadow(g, "MARIO " + df.format(Mario.lives), 0, 0, 7);
		drawStringDropShadow(g, "00000000", 0, 1, 7);

		drawStringDropShadow(g, "COIN", 14, 0, 7);
		drawStringDropShadow(g, " " + df.format(Mario.coins), 14, 1, 7);

		drawStringDropShadow(g, "WORLD", 24, 0, 7);
		drawStringDropShadow(g, " " + Mario.levelString, 24, 1, 7);

		drawStringDropShadow(g, "TIME", 35, 0, 7);
		int time = (timeLeft + 15 - 1) / 15;
		if (time < 0)
			time = 0;
		drawStringDropShadow(g, " " + df2.format(time), 35, 1, 7);

		// if(tick/8%2==0) {
		// drawStringDropShadow(g,
		// Float.toString(Mario.bloodSugar), 0, 27, 1);
		if (Mario.displayedHelp == false)
			DrawHelp(g);
		if (gameModeDibetic) {
			drawSugarLevel(g);

			// }
			if (drawMainMenu)
				drawMainMenu(g);
			if (drawNormalFoodMenu)
				drawFoodMenu(g);
			if (drawFeedback)
				drawFeedbackScreen(g);

			if (drawUrgentFoodMenu)
				drawInsulinMenu(g);
		}
		if (startTime > 0) {
			float t = startTime + alpha - 2;
			t = t * t * 0.6f;
			//renderBlackout(g, 160, 120, (int) (t));
		}
		// mario.x>level.xExit*16
		if (mario.winTime > 0) {
			float t = mario.winTime + alpha;
			t = t * t * 0.2f;

			if (t > 900) {
				renderer.levelWon();
				// replayer = new Replayer(recorder.getBytes());
				// init();
			}

			//renderBlackout(g, (int) (mario.xDeathPos - xCam),
			//		(int) (mario.yDeathPos - yCam), (int) (320 - t));
		}

		if (mario.deathTime > 0) {
			float t = mario.deathTime + alpha;
			t = t * t * 0.4f;

			if (t > 1800) {
				renderer.levelFailed();
				// replayer = new Replayer(recorder.getBytes());
				// init();
			}

			//renderBlackout(g, (int) (mario.xDeathPos - xCam),
			//		(int) (mario.yDeathPos - yCam), (int) (320 - t));
		}

	}

	private void drawFeedbackScreen(Canvas g) {

		drawStringDropShadow(g, feedbackMessageLine1, 0, 4, 1);
		drawStringDropShadow(g, feedbackMessageLine2, 0, 5, 1);
		drawStringDropShadow(g, feedbackMessageLine3, 0, 6, 1);
		drawStringDropShadow(g, "Press Enter to continue...", 5, 7, 1);
		if (mario.keys[Mario.KEY_ENTER]) {
			drawFeedback = false;
			mario.keys[Mario.KEY_ENTER] = false;
			paused = false;
		}
	}

	private void UpdateHelp() {
		paused = true;
		for (int i = 0; i < mario.keys.length; i++)
			if (mario.keys[i] == true) {
				mario.keys[i] = false;
				Mario.displayedHelp = true;
				paused = false;
			}

	}

	private void DrawHelp(Canvas g) {

		drawStringDropShadow(g, "Mario has diabetes and a coloured", 0, 4, 1);
		drawStringDropShadow(g, "bar indicates his health.", 0, 5, 2);
		drawStringDropShadow(g, "When the indicator is green ", 0, 6, 3);
		drawStringDropShadow(g, "Mario is healthy but Marios ", 0, 7, 4);
		drawStringDropShadow(g, "blood sugar level will drop ", 0, 8, 5);
		drawStringDropShadow(g, "with all the running and jumping ", 0, 9, 6);
		drawStringDropShadow(g, "he has to do.  As his health", 0, 10, 1);
		drawStringDropShadow(g, "gets worse the indicator turns red.", 0, 11, 2);
		drawStringDropShadow(g, "Mario will need to eat to maintain ", 0, 12, 3);
		drawStringDropShadow(g, "his blood-sugar level or take", 0, 13, 4);
		drawStringDropShadow(g, "insulin to reduce this if it", 0, 14, 5);
		drawStringDropShadow(g, "gets too high.", 0, 15, 6);
		// drawStringDropShadow(g,
		// "The food menu has foods to raise your blood sugar", 0, 8,3);
		// drawStringDropShadow(g,
		// "The food menu has foods to raise your blood sugar", 0, 8,3);
		drawStringDropShadow(g, "Press any key to play...", 16, 10, 1);

	}

	Food[] normalFoods;
	Food[] urgentFoods;

	private void loadData() {
//TODO
		int numberOfNormalFoods = Integer.parseInt(INIManager
				.ReadValue("numberOfNormalFoods"));
		// System.out.println(numberOfNormalFoods);
		normalFoods = new Food[numberOfNormalFoods];

		for (int i = 1; i < numberOfNormalFoods + 1; i++) {
			// System.out.println(i);
			// System.out.println(INIManager.ReadValue("normalFood" + i +
			// "Name"));
			normalFoods[i - 1] = new Food(INIManager.ReadValue("normalFood" + i
					+ "Name"), Float.parseFloat(INIManager
					.ReadValue("normalFood" + i + "BSValue")));
			/*try {
				// File foodIconFile = new File(INIManager.ReadValue("food" + i
				// + "Icon"));

				//normalFoods[i - 1].Icon = Art.getImageTwo(
				//		graphicsConfiguration,
				//		INIManager.ReadValue("normalFood" + i + "Icon"));
			} catch (IOException e) {

				e.printStackTrace();
			}
			*/

		}

		int numberOfUrgentFoods = Integer.parseInt(INIManager
				.ReadValue("numberOfUrgentFoods"));
		// System.out.println(numberOfUrgentFoods);
		urgentFoods = new Food[numberOfUrgentFoods];

		for (int i = 1; i < numberOfUrgentFoods + 1; i++) {
			// System.out.println(i);
			// System.out.println(INIManager.ReadValue("urgentFood" + i +
			// "Name"));
			urgentFoods[i - 1] = new Food(INIManager.ReadValue("urgentFood" + i
					+ "Name"), Float.parseFloat(INIManager
					.ReadValue("urgentFood" + i + "BSValue")));
			/*try {
				// File foodIconFile = new File(INIManager.ReadValue("food" + i
				// + "Icon"));

				//urgentFoods[i - 1].Icon = Art.getImageTwo(
				//		graphicsConfiguration,
				//		INIManager.ReadValue("urgentFood" + i + "Icon"));
			} catch (IOException e) {

				e.printStackTrace();
			}
			*/

		}

		if (Mario.loadedBloodSugar == false) {
			Mario.bloodSugar = Float.parseFloat(INIManager
					.ReadValue("StartBloodSugar"));
			Mario.loadedBloodSugar = true;
		}
		idleBloodSugarDrop = Float.parseFloat(INIManager
				.ReadValue("idleBloodSugarDrop"));
		walkingBloodSugarDrop = Float.parseFloat(INIManager
				.ReadValue("walkingBloodSugarDrop"));
		runningBloodSugarDrop = Float.parseFloat(INIManager
				.ReadValue("runningBloodSugarDrop"));
		jumpBloodSugarDrop = Float.parseFloat(INIManager
				.ReadValue("jumpBloodSugarDrop"));

		maxBloodSugarSafe = Float.parseFloat(INIManager
				.ReadValue("maxBloodSugarSafe"));
		minBloodSugarSafe = Float.parseFloat(INIManager
				.ReadValue("minBloodSugarSafe"));

		maxBloodSugarWarning = Float.parseFloat(INIManager
				.ReadValue("maxBloodSugarWarning"));
		minBloodSugarWarning = Float.parseFloat(INIManager
				.ReadValue("minBloodSugarWarning"));

		maxBloodSugarCritical = Float.parseFloat(INIManager
				.ReadValue("maxBloodSugarCritical"));
		minBloodSugarCritical = Float.parseFloat(INIManager
				.ReadValue("minBloodSugarCritical"));

		CriticalBloodSugarColor = Integer.parseInt(INIManager
				.ReadValue("CriticalBloodSugarColor"));
		WarningBloodSugarColor = Integer.parseInt(INIManager
				.ReadValue("WarningBloodSugarColor"));
		SafeBloodSugarColor = Integer.parseInt(INIManager
				.ReadValue("SafeBloodSugarColor"));

		feedbackLevel = Float
				.parseFloat(INIManager.ReadValue("feedback1Level"));

		feedbackMessageLine1 = INIManager.ReadValue("feedback1MessageLine1");
		feedbackMessageLine2 = INIManager.ReadValue("feedback1MessageLine2");
		feedbackMessageLine3 = INIManager.ReadValue("feedback1MessageLine3");
	}

	int foodMenuCounterPos = 0;
	boolean scrolledNormalFood = false;
	int mainMenuCounterPos = 0;
	boolean scrolledMainMenu = false;
	int urgentFoodMenuCounterPos = 0;
	boolean scrolledUrgentFoodMenu = false;

	private void updateMainMenu() {
		// updateFoodMenuItems();
		// scrolledFood = false;
		if (drawNormalFoodMenu)
			drawMainMenu = false;
		if (drawUrgentFoodMenu)
			drawMainMenu = false;
		if (mario.keys[Mario.KEY_DOWN]) {
			if (!scrolledMainMenu) {
				scrolledMainMenu = true;
				mainMenuCounterPos++;
			}
		} else if (mario.keys[Mario.KEY_UP]) {
			if (!scrolledMainMenu) {
				scrolledMainMenu = true;
				mainMenuCounterPos--;
			}
		} else
			scrolledMainMenu = false;
		if (mainMenuCounterPos > 2)
			mainMenuCounterPos = 0;
		if (mainMenuCounterPos < 0)
			mainMenuCounterPos = 2;
		if (mario.keys[Mario.KEY_CONFIRM]) {
			mario.keys[Mario.KEY_CONFIRM] = false;
			if (mainMenuCounterPos == 0) {
				drawNormalFoodMenu = true;
				drawMainMenu = false;
				CSVLogger.LogUserSelectedMenu(name, sessionNum,
						Mario.bloodSugar, "Normal Food Menu");
			}
			if (mainMenuCounterPos == 1) {
				drawUrgentFoodMenu = true;
				drawMainMenu = false;
				CSVLogger.LogUserSelectedMenu(name, sessionNum,
						Mario.bloodSugar, "Urgent Food Menu");
			}
			if (mainMenuCounterPos == 2) {
				drawMainMenu = false;
				paused = false;
			}
		}
	}

	private void updateFoodMenuItems() {
		/*
		 * if (Mario.bloodSugar < minBloodSugar) { for (int i = 0; i <
		 * foods.length; i++) { if (foods[i].bloodSugarArmount < 0) {
		 * foods[i].enabled = false; foods[i].color = 8; } if
		 * (foods[i].bloodSugarArmount > 0) { foods[i].enabled = true;
		 * foods[i].color = 3; } }
		 * 
		 * } else if (Mario.bloodSugar > maxBloodSugar) { for (int i = 0; i <
		 * foods.length; i++) { if (foods[i].bloodSugarArmount > 0) {
		 * foods[i].enabled = false; foods[i].color = 8; } if
		 * (foods[i].bloodSugarArmount < 0) { foods[i].enabled = true;
		 * foods[i].color = 3; } } } else if (Mario.bloodSugar < maxBloodSugar
		 * || Mario.bloodSugar > minBloodSugar) { for (int i = 0; i <
		 * foods.length; i++) { foods[i].enabled = true; foods[i].color = 3; }
		 * 
		 * }
		 */
	}

	private void updateNormalFoodMenu() {
		updateFoodMenuItems();
		// scrolledFood = false;
		if (drawMainMenu)
			drawNormalFoodMenu = false;
		if (drawUrgentFoodMenu)
			drawNormalFoodMenu = false;
		if (mario.keys[Mario.KEY_DOWN]) {
			if (!scrolledNormalFood) {
				scrolledNormalFood = true;
				foodMenuCounterPos++;
			}
		} else if (mario.keys[Mario.KEY_UP]) {
			if (!scrolledNormalFood) {
				scrolledNormalFood = true;
				foodMenuCounterPos--;
			}
		} else
			scrolledNormalFood = false;
		if (foodMenuCounterPos > normalFoods.length)
			foodMenuCounterPos = 0;
		if (foodMenuCounterPos < 0)
			foodMenuCounterPos = normalFoods.length;
		if (mario.keys[Mario.KEY_CONFIRM]) {
			if (foodMenuCounterPos == normalFoods.length) {
				if (Mario.bloodSugar > minBloodSugarCritical) {
					drawNormalFoodMenu = false;
					drawMainMenu = true;
					mario.keys[Mario.KEY_CONFIRM] = false;
				}
			} else if (normalFoods[foodMenuCounterPos].enabled) {
				CSVLogger
						.LogFoodChosen(
								name,
								sessionNum,
								normalFoods[foodMenuCounterPos].foodName,
								Mario.bloodSugar,
								Mario.bloodSugar += normalFoods[foodMenuCounterPos].bloodSugarAmount);
				Mario.bloodSugar += normalFoods[foodMenuCounterPos].bloodSugarAmount;
				mario.keys[Mario.KEY_CONFIRM] = false;
				drawNormalFoodMenu = false;
				paused = false;
			}

		}
	}

	private void updateUrgentFoodMenu() {
		if (drawNormalFoodMenu)
			drawUrgentFoodMenu = false;
		if (drawMainMenu)
			drawUrgentFoodMenu = false;
		if (mario.keys[Mario.KEY_DOWN]) {
			if (!scrolledUrgentFoodMenu) {
				scrolledUrgentFoodMenu = true;
				urgentFoodMenuCounterPos++;
			}
		} else if (mario.keys[Mario.KEY_UP]) {
			if (!scrolledUrgentFoodMenu) {
				scrolledUrgentFoodMenu = true;
				urgentFoodMenuCounterPos--;
			}
		} else
			scrolledUrgentFoodMenu = false;
		if (urgentFoodMenuCounterPos > urgentFoods.length)
			urgentFoodMenuCounterPos = 0;
		if (urgentFoodMenuCounterPos < 0)
			urgentFoodMenuCounterPos = urgentFoods.length;
		if (mario.keys[Mario.KEY_CONFIRM]) {
			if (urgentFoodMenuCounterPos == urgentFoods.length) {
				if (Mario.bloodSugar > minBloodSugarCritical) {
					drawUrgentFoodMenu = false;
					drawMainMenu = true;
					mario.keys[Mario.KEY_CONFIRM] = false;
				}
			} else if (urgentFoods[urgentFoodMenuCounterPos].enabled) {
				CSVLogger
						.LogFoodChosen(
								name,
								sessionNum,
								urgentFoods[urgentFoodMenuCounterPos].foodName,
								Mario.bloodSugar,
								Mario.bloodSugar += urgentFoods[urgentFoodMenuCounterPos].bloodSugarAmount);
				Mario.bloodSugar += urgentFoods[urgentFoodMenuCounterPos].bloodSugarAmount;
				mario.keys[Mario.KEY_CONFIRM] = false;
				drawUrgentFoodMenu = false;
				paused = false;
			}

		}
		/*
		 * if (drawNormalFoodMenu) drawUrgentFoodMenu = false; if (drawMainMenu)
		 * drawUrgentFoodMenu = false; // scrolledFood = false; if
		 * (mario.keys[Mario.KEY_DOWN]) { if (!scrolledInsulinMenu) {
		 * scrolledInsulinMenu = true; insulinMeunCounterPos++; } } else if
		 * (mario.keys[Mario.KEY_UP]) { if (!scrolledInsulinMenu) {
		 * scrolledInsulinMenu = true; insulinMeunCounterPos--; } } else
		 * scrolledInsulinMenu = false; if (insulinMeunCounterPos > 6)
		 * insulinMeunCounterPos = 0; if (insulinMeunCounterPos < 0)
		 * insulinMeunCounterPos = 6; if (mario.keys[Mario.KEY_CONFIRM]) { if
		 * (insulinMeunCounterPos < 5) { CSVLogger.LogInsulinTaken(name,
		 * sessionNum, insulinMeunCounterPos + 1, Mario.bloodSugar);
		 * Mario.bloodSugar -= insulinMeunCounterPos + 1;
		 * 
		 * drawUrgentFoodMenu = false; mario.keys[Mario.KEY_CONFIRM] = false;
		 * paused = false; } if (insulinMeunCounterPos == 5) if
		 * (Mario.bloodSugar < maxCriticalBloodSugarMax) { drawMainMenu = true;
		 * mario.keys[Mario.KEY_CONFIRM] = false; drawUrgentFoodMenu = false; }
		 * }
		 */

	}

	private void drawMainMenu(Canvas g) {
		drawStringDropShadow(g, ">", 8, mainMenuCounterPos + 7, 1);
		drawStringDropShadow(g, "Normal Food Menu", 10, 7, 3);
		drawStringDropShadow(g, "Urgent Food Menu", 10, 8, 3);
		drawStringDropShadow(g, "Exit", 10, 9, 3);
	}

	private void drawInsulinMenu(Canvas g) {
		drawStringDropShadow(g, ">", 8, urgentFoodMenuCounterPos + 7, 1);
		for (int i = 0; i < urgentFoods.length; i++) {
			drawStringDropShadow(g, urgentFoods[i].foodName, 10, i + 7,
					urgentFoods[i].color);

			if (urgentFoods[i].Icon != null)
				g.drawBitmap(urgentFoods[i].Icon, null, new Rect( 76, (i + 7) * 8 + 5, 8, 8),
						null);
		}
		drawStringDropShadow(g, "Exit", 10, urgentFoods.length + 7, 3);
		/*
		 * drawStringDropShadow(g, ">", 8, insulinMeunCounterPos + 7, 1);
		 * drawStringDropShadow(g, "1ml Insulin", 10, 7, 3);
		 * drawStringDropShadow(g, "2ml Insulin", 10, 8, 3);
		 * drawStringDropShadow(g, "3ml Insulin", 10, 9, 3);
		 * drawStringDropShadow(g, "4ml Insulin", 10, 10, 3);
		 * drawStringDropShadow(g, "5ml Insulin", 10, 11, 3); if
		 * (Mario.bloodSugar > maxCriticalBloodSugarMax) drawStringDropShadow(g,
		 * "Exit", 10, 12, 8);
		 * 
		 * else drawStringDropShadow(g, "Exit", 10, 12, 3);
		 */
	}

	private void drawFoodMenu(Canvas g) {
		drawStringDropShadow(g, ">", 8, foodMenuCounterPos + 7, 1);
		for (int i = 0; i < normalFoods.length; i++) {
			drawStringDropShadow(g, normalFoods[i].foodName, 10, i + 7,
					normalFoods[i].color);

			if (normalFoods[i].Icon != null)
				g.drawBitmap(normalFoods[i].Icon, null,new Rect( 76, (i + 7) * 8 + 5, 8, 8),
						null);
			// drawStringDropShadow(g, Character.toString(foods[i].icon), 9,
			// i + 7, 9);
			// drawStringDropShadow(g, "(+" + foods[i].bloodSugarArmount +
			// "bs)",
			// 30, i + 7, 3);
		}
		// if (Mario.bloodSugar < minBloodSugar)
		// drawStringDropShadow(g, "Exit", 10, 12, 8);
		// else
		drawStringDropShadow(g, "Exit", 10, normalFoods.length + 7, 3);
	}

	private boolean Between(float num, float min, float max) {
		if (num > min && num < max)
			return true;
		else
			return false;

	}

	// private float calcSL(int timesBy) {
	// return maxBloodSugar / 19 * timesBy;
	// }

	private void drawSugarLevel(Canvas g) {

		int bsNum = 9;

		/*
		 * float minCriticalBloodSugarMin; float minCriticalBloodSugarMax;
		 * 
		 * float minWarningBloodSugarMin; float minWarningBloodSugarMax;
		 * 
		 * float minSafeBloodSugarMin; float minSafeBloodSugarMax;
		 * 
		 * float maxWarningBloodSugarMin; float maxWarningBloodSugarMax;
		 * 
		 * float maxCriticalBloodSugarMin; float maxCriticalBloodSugarMax;
		 * 
		 * int CriticalBloodSugarColor;; int WarningBloodSugarColor; int
		 * SafeBloodSugarColor;
		 */
		float tempValue = Mario.bloodSugar / this.maxBloodSugarCritical;
		
		tempValue = tempValue * 18;
		bsNum = (int) tempValue;
		
//			Log.e("bloodSugar",Double.toString(Mario.bloodSugar));
//			Log.e("bsNumr",Double.toString(bsNum));
		
		DrawBloodSugarBar(g, bsNum);
		/*
		 * if (Mario.bloodSugar > minBloodSugarSafe) bsNum =
		 * SafeBloodSugarColor;
		 * 
		 * if (Between(Mario.bloodSugar, minBloodSugarWarning,
		 * maxBloodSugarWarning)) bsNum = WarningBloodSugarColor;
		 * 
		 * if (Between(Mario.bloodSugar, minBloodSugarCritical,
		 * maxBloodSugarCritical)) bsNum = CriticalBloodSugarColor;
		 * 
		 * DrawBloodSugarBar(g, bsNum); // /drawString(g, // "0123456789", x * 8
		 * + 5, y * 8 + 5, 9); // bsNum = bloodSugar; /* if (Mario.bloodSugar >
		 * maxBloodSugar) bsNum = 9; if (Between(Mario.bloodSugar, calcSL(18),
		 * calcSL(19))) bsNum = 9; if (Between(Mario.bloodSugar, calcSL(17),
		 * calcSL(18))) bsNum = 8; if (Between(Mario.bloodSugar, calcSL(16),
		 * calcSL(17))) bsNum = 8; if (Between(Mario.bloodSugar, calcSL(15),
		 * calcSL(16))) bsNum = 7; if (Between(Mario.bloodSugar, calcSL(14),
		 * calcSL(15))) bsNum = 7; if (Between(Mario.bloodSugar, calcSL(13),
		 * calcSL(14))) bsNum = 6; if (Between(Mario.bloodSugar, calcSL(12),
		 * calcSL(13))) bsNum = 6; if (Between(Mario.bloodSugar, calcSL(11),
		 * calcSL(12))) bsNum = 5;
		 * 
		 * if (Between(Mario.bloodSugar, calcSL(10), calcSL(11))) bsNum = 5; if
		 * (Between(Mario.bloodSugar, calcSL(9), calcSL(10))) bsNum = 4; if
		 * (Between(Mario.bloodSugar, calcSL(8), calcSL(9))) bsNum = 4; if
		 * (Between(Mario.bloodSugar, calcSL(7), calcSL(8))) bsNum = 3; if
		 * (Between(Mario.bloodSugar, calcSL(6), calcSL(7))) bsNum = 3; if
		 * (Between(Mario.bloodSugar, calcSL(5), calcSL(6))) bsNum = 2; if
		 * (Between(Mario.bloodSugar, calcSL(4), calcSL(5))) bsNum = 2; if
		 * (Between(Mario.bloodSugar, calcSL(3), calcSL(4))) bsNum = 1; if
		 * (Between(Mario.bloodSugar, calcSL(2), calcSL(3))) bsNum = 1; if
		 * (Mario.bloodSugar < minBloodSugar) bsNum = 0; DrawBloodSugarBar(g,
		 * bsNum); // drawString(g, // df3.format(bsNum), x * 8 + 5, y * 8 + 5 ,
		 * 9);
		 */
	}

	private void DrawBloodSugarBar(Canvas g, int bsNum) {
		// clear the old bar
		for (int i = 0; i < 15; i++) {

			drawString(g, "/", i * 8 + 5, 2 * 8 + 5, 9);
		}

		
		for (int i = 0; i < bsNum + 1; i++) {
			g.drawBitmap(Art.font[16+i][9],  i * 8 + 5 , 2 * 8 + 5, null);
			
//			drawString(g, df3.format(bsNum), i * 8 + 5, 2 * 8 + 5, 9);
		}

	}

	private void drawStringDropShadow(Canvas g, String text, int x, int y,
			int c) {
		drawString(g, text, x * 8 + 5, y * 8 + 5, 0);
		drawString(g, text, x * 8 + 4, y * 8 + 4, c);
	}

	private void drawBlock(Canvas g, int block, int x, int y, int row) {
		

			g.drawBitmap(Art.font[block][row], x , y, null);
	}
	
	private void drawString(Canvas g, String text, int x, int y, int row) {
		char[] ch = text.toCharArray();
		for (int i = 0; i < ch.length; i++) {
			g.drawBitmap(Art.font[ch[i] - 32][row], x + i * 8, y, null);
		}
	}

	private void renderBlackout(Canvas g, int x, int y, int radius) {
		if (radius > 320)
			return;

		int[] xp = new int[20];
		int[] yp = new int[20];
		for (int i = 0; i < 16; i++) {
			xp[i] = x + (int) (Math.cos(i * Math.PI / 15) * radius);
			yp[i] = y + (int) (Math.sin(i * Math.PI / 15) * radius);
		}
		xp[16] = 320;
		yp[16] = y;
		xp[17] = 320;
		yp[17] = 240;
		xp[18] = 0;
		yp[18] = 240;
		xp[19] = 0;
		yp[19] = y;
		//g.fillPolygon(xp, yp, xp.length);

		for (int i = 0; i < 16; i++) {
			xp[i] = x - (int) (Math.cos(i * Math.PI / 15) * radius);
			yp[i] = y - (int) (Math.sin(i * Math.PI / 15) * radius);
		}
		xp[16] = 320;
		yp[16] = y;
		xp[17] = 320;
		yp[17] = 0;
		xp[18] = 0;
		yp[18] = 0;
		xp[19] = 0;
		yp[19] = y;

		//g.fillPolygon(xp, yp, xp.length);
		//TODO fix blackout
	}

	public void addSprite(Sprite sprite) {
		spritesToAdd.add(sprite);
		sprite.tick();
	}

	public void removeSprite(Sprite sprite) {
		spritesToRemove.add(sprite);
	}

	public float getX(float alpha) {
		int xCam = (int) (mario.xOld + (mario.x - mario.xOld) * alpha) - 160;
		// int yCam = (int) (mario.yOld + (mario.y - mario.yOld) * alpha) - 120;
		// int xCam = (int) (xCamO + (this.xCam - xCamO) * alpha);
		// int yCam = (int) (yCamO + (this.yCam - yCamO) * alpha);
		if (xCam < 0)
			xCam = 0;
		// if (yCam < 0) yCam = 0;
		// if (yCam > 0) yCam = 0;
		return xCam + 160;
	}

	public float getY(float alpha) {
		return 0;
	}

	public void bump(int x, int y, boolean canBreakBricks) {
		byte block = level.getBlock(x, y);

			if ((Level.TILE_BEHAVIORS[block & 0xff] & Level.BIT_BUMPABLE) > 0) {
				bumpInto(x, y - 1);
				level.setBlock(x, y, (byte) 4);
				level.setBlockData(x, y, (byte) 4);
	
				if (((Level.TILE_BEHAVIORS[block & 0xff]) & Level.BIT_SPECIAL) > 0)
				{
					AndroidSoundEngine.play(R.raw.sprout);
					
					addSprite(new Mushroom(this, x * 16 + 8, y * 16 + 8));
				}
				else
				{
					Mario.getCoin();
					AndroidSoundEngine.play(R.raw.coin);
					//sound.play(Art.samples[Art.SAMPLE_GET_COIN],
						//	new FixedSoundSource(x * 16 + 8, y * 16 + 8), 1, 1, 1);
					addSprite(new CoinAnim(this, x, y));
				}
			}
	
			if ((Level.TILE_BEHAVIORS[block & 0xff] & Level.BIT_BREAKABLE) > 0) {
				bumpInto(x, y - 1);
				if (canBreakBricks) {
					AndroidSoundEngine.play(R.raw.breakblock);
					//sound.play(Art.samples[Art.SAMPLE_BREAK_BLOCK],
					//		new FixedSoundSource(x * 16 + 8, y * 16 + 8), 1, 1, 1);
					level.setBlock(x, y, (byte) 0);
					for (int xx = 0; xx < 2; xx++)
						for (int yy = 0; yy < 2; yy++)
							addSprite(new Particle(x * 16 + xx * 8 + 4, y * 16 + yy
									* 8 + 4, (xx * 2 - 1) * 4, (yy * 2 - 1) * 4 - 8));
				} else {
						level.setBlockData(x, y, (byte) 4);
			}
		}
	}

	public void bumpInto(int x, int y) {
		byte block = level.getBlock(x, y);
		if (((Level.TILE_BEHAVIORS[block & 0xff]) & Level.BIT_PICKUPABLE) > 0) {
			Mario.getCoin();
			AndroidSoundEngine.play(R.raw.coin);
			//sound.play(Art.samples[Art.SAMPLE_GET_COIN], new FixedSoundSource(
			//		x * 16 + 8, y * 16 + 8), 1, 1, 1);
			level.setBlock(x, y, (byte) 0);
			addSprite(new CoinAnim(this, x, y + 1));
		}

		for (Sprite sprite : sprites) {
			sprite.bumpCheck(x, y);
		}
	}

	public LevelRenderer getLayer() {
		return layer;
	}
}