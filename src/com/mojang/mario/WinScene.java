package com.mojang.mario;

//import java.awt.Color;
//import java.awt.Graphics;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import com.mojang.mario.sprites.Mario;

public class WinScene extends Scene
{
    private MarioComponent component;
    private int tick;
    private String scrollMessage = "Thank you for saving me, Mario!";
    
    public WinScene(MarioComponent component)
    {
        this.component = component;
    }

    public void init()
    {
    	component.getActivity().runOnUiThread(new Runnable(){
    		public void run(){
		    	for(View control: component.getControls())
		    	{
		    		control.setVisibility(View.INVISIBLE);
		     		control.setEnabled(false);
		    	}
    		}
    	});

    }

    public void render(Canvas g, float alpha, Paint paint)
    {
        g.drawColor(Color.rgb(128 ,128, 160));//decode("#8080a0"));
        //g.fillRect(0, 0, 320, 240);
        g.drawBitmap(Art.endScene[tick/24%2][0], 160-48, 100-48, null);
        drawString(g, scrollMessage, 160-scrollMessage.length()*4, 160, 0);
    }

    private void drawString(Canvas g, String text, int x, int y, int c)
    {
        char[] ch = text.toCharArray();
        for (int i = 0; i < ch.length; i++)
        {
            g.drawBitmap(Art.font[ch[i] - 32][c], x + i * 8, y, null);
        }
    }

    private boolean wasDown = true;
    public void tick()
    {
        tick++;
        if (!wasDown && keys[Mario.KEY_JUMP])
        {
            component.toTitle();
        }
        if (keys[Mario.KEY_JUMP])
        {
            wasDown = false;
        }
    }

    public float getX(float alpha)
    {
        return 0;
    }

    public float getY(float alpha)
    {
        return 0;
    }
}
