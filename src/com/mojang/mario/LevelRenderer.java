package com.mojang.mario;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.Log;

import com.mojang.mario.level.Level;


public class LevelRenderer
{
    private int xCam;
    private int yCam;
    private Bitmap image;
    private Canvas g;
    private Level level;

    public boolean renderBehaviors = false;

    int width;
    int height;

    public LevelRenderer(Level level, int width, int height)
    {
        this.width = width;
        this.height = height;

        this.level = level;
        image = Bitmap.createBitmap(width, height, Config.RGB_565);
        g = new Canvas(image);

        updateArea(0, 0, width, height);
    }

    Bitmap tmp;

	public void setCam(int xCam, int yCam)
    {
        int xCamD = this.xCam - xCam;
        int yCamD = this.yCam - yCam;
        this.xCam = xCam;
        this.yCam = yCam;

        tmp = Bitmap.createBitmap(image);
        g.drawColor(Color.BLACK);
        g.drawBitmap(tmp, xCamD,yCam, null);
        tmp.recycle();

        if (xCamD < 0)
        {
            if (xCamD < -width) xCamD = -width;
            updateArea(width + xCamD, 0, -xCamD, height);
        }
        else if (xCamD > 0)
        {
            if (xCamD > width) xCamD = width;
            updateArea(0, 0, xCamD, height);
        }

        if (yCamD < 0)
        {
            if (yCamD < -width) yCamD = -width;
            updateArea(0, height + yCamD, width, -yCamD);
        }
        else if (yCamD > 0)
        {
            if (yCamD > width) yCamD = width;
            updateArea(0, 0, width, yCamD);
        }
    }

    public void clearArea(int x0, int y0, int w, int h)
    {
        int xTileStart = (x0 + xCam) / 16;
        int yTileStart = (y0 + yCam) / 16;
        int xTileEnd = (x0 + xCam + w) / 16;
        int yTileEnd = (y0 + yCam + h) / 16;
        
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        g.drawRect(xTileStart, yTileStart, xTileEnd, yTileEnd, paint);
    }
    
    
    public void updateArea(int x0, int y0, int w, int h)
    {
        int xTileStart = (x0 + xCam) / 16;
        int yTileStart = (y0 + yCam) / 16;
        int xTileEnd = (x0 + xCam + w) / 16;
        int yTileEnd = (y0 + yCam + h) / 16;
        for (int x = xTileStart; x <= xTileEnd; x++)
        {
            for (int y = yTileStart; y <= yTileEnd; y++)
            {
                int b = level.getBlock(x, y) & 0xff;
                if (((Level.TILE_BEHAVIORS[b]) & Level.BIT_ANIMATED) == 0)
                {                	
                    g.drawBitmap(Art.level[b % 16][b / 16], (x << 4) - xCam, (y << 4) - yCam, null);
                    
                }
            }
        }
     }
    
    public void render(Canvas g, int tick, float alpha)
    {
        g.drawBitmap(image, 0, 0, null);

        for (int x = xCam / 16; x <= (xCam + width) / 16; x++)
            for (int y = yCam / 16; y <= (yCam + height) / 16; y++)
            {
                byte b = level.getBlock(x, y);

                int yo = 0;
                if (x >= 0 && y >= 0 && x < level.width && y < level.height) yo = level.data[x][y];
                if (yo > 0)
                {
                	yo = (int) (Math.sin((yo - alpha) / 4.0f * Math.PI) * 8);
        		}

                
                if (((Level.TILE_BEHAVIORS[b & 0xff]) & Level.BIT_ANIMATED) > 0)
                {
                    int animTime = (tick / 3) % 4;

                    if ((b % 16) / 4 == 0 && b / 16 == 1)
                    {
                        animTime = (tick / 2 + (x + y) / 8) % 20;
                        if (animTime > 3) animTime = 0;
                    }
                    if ((b % 16) / 4 == 3 && b / 16 == 0)
                    {
                        animTime = 2;
                    }
                    int row = b / 16;
                    int col = (b % 16) / 4 * 4 + animTime;
                    g.drawBitmap(Art.level[col][row], (x << 4) - xCam, (y << 4) - yCam - yo, null);
                }
                else if (b == 1 )
                {
            		Paint paint = new Paint();
            		paint.setColor(Color.BLACK);
            		paint.setStyle(Style.FILL);
                	
                	g.drawRect((x << 4) - xCam, (y << 4) - yCam, (x << 4) - xCam + 16, (y << 4) - yCam+16,paint);
                    g.drawBitmap(Art.level[0][0], (x << 4) - xCam, (y << 4) - yCam - yo, null);
                	
                }
            }
    }

    public void setLevel(Level level)
    {
        this.level = level;
        updateArea(0, 0, width, height);
    }

    public void renderExit0(Canvas g, int tick, float alpha, boolean bar)
    {
        for (int y = level.yExit - 8; y < level.yExit; y++)
        {
            g.drawBitmap(Art.level[12][y == level.yExit - 8 ? 4 : 5], (level.xExit << 4) - xCam - 16, (y << 4) - yCam, null);
        }
        int yh = level.yExit * 16 - (int) ((Math.sin((tick + alpha) / 20) * 0.5 + 0.5) * 7 * 16) - 8;
        if (bar)
        {
            g.drawBitmap(Art.level[12][3], (level.xExit << 4) - xCam - 16, yh - yCam, null);
            g.drawBitmap(Art.level[13][3], (level.xExit << 4) - xCam, yh - yCam, null);
        }
    }


    public void renderExit1(Canvas g, int tick, float alpha)
    {
        for (int y = level.yExit - 8; y < level.yExit; y++)
        {
            g.drawBitmap(Art.level[13][y == level.yExit - 8 ? 4 : 5], (level.xExit << 4) - xCam + 16, (y << 4) - yCam, null);
        }
    }

	public int getxCam() {
		return xCam;
	}

	public int getyCam() {
		return yCam;
	}
}