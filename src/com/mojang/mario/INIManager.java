package com.mojang.mario;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager.AssetInputStream;

public class INIManager {

	
	//static File settingsFile;// = new File("Settings.ini"); 
	static boolean loaded = false;
	
	static Properties prop = new Properties();
	
	public static Context context;
	
	public static String ReadValue(String name)
	{
		if (!loaded)
			try {
				loadProcedure();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*if(!settingsFile.exists()){ 
				try { 
					settingsFile.createNewFile(); 
					FileOutputStream out = new FileOutputStream(settingsFile);
					AddDefaultVlaues(out); 
					
					out.flush();  
					out.close(); 
				} catch (IOException ex) { 
					ex.printStackTrace();
				}
			} else { 
					try {
						loadProcedure();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}*/
		return prop.getProperty(name);	
		
	}
	static void AddDefaultVlaues(FileOutputStream out) throws IOException
	{
		prop.put("numberOfNormalFoods", "3");
		prop.put("numberOfUrgentFoods", "2");
		
		prop.put("normalFood1Name", "Sandwich");
		prop.put("normalFood1BSValue", "0.5f");
		prop.put("normalFood1Icon", "Icons\\Sandwich.png");
		
		
		prop.put("normalFood2Name", "Juice");
		prop.put("normalFood2BSValue", "0.5f");
		prop.put("normalFood2Icon", "Icons\\Juice.png");
			
		prop.put("normalFood3Name", "Apple");
		prop.put("normalFood3BSValue", "0.5f");
		prop.put("normalFood3Icon", "Icons\\Apple.png");
			
		prop.put("urgentFood1Name", "Yogurt");
		prop.put("urgentFood1BSValue", "2f");
		prop.put("urgentFood1Icon", "Icons\\Yogurt.png");
			
		prop.put("urgentFood2Name", "Burger");
		prop.put("urgentFood2BSValue", "2f");
		prop.put("urgentFood2Icon", "Icons\\Burger.png");
		
		prop.put("StartBloodSugar", "7f");
		
		prop.put("idleBloodSugarDrop", "0.005f");
		prop.put("walkingBloodSugarDrop", "0.02f");
		prop.put("runningBloodSugarDrop", "0.06f");
		prop.put("jumpBloodSugarDrop", "0.09f");
		
		prop.put("maxBloodSugarSafe", "7");
		prop.put("minBloodSugarSafe", "5.5");
		
		prop.put("maxBloodSugarWarning", "5.4");
		prop.put("minBloodSugarWarning", "4.1");
		
		prop.put("maxBloodSugarCritical", "4");
		prop.put("minBloodSugarCritical", "1");
		
		prop.put("feedback1Level", "3.0f");
		prop.put("feedback1MessageLine1", "Mario's blood sugar level is");
		prop.put("feedback1MessageLine2", "well below the normal range");
		prop.put("feedback1MessageLine3", "he needs to eat something !!");
		/*
		
		prop.put("minCriticalBloodSugarMin", "0");
		prop.put("minCriticalBloodSugarMax", "2");
		
		prop.put("minWarningBloodSugarMin", "2");
		prop.put("minWarningBloodSugarMax", "4");
		
		prop.put("minSafeBloodSugarMin", "4");
		prop.put("minSafeBloodSugarMax", "8");
		
		prop.put("maxWarningBloodSugarMin", "8");
		prop.put("maxWarningBloodSugarMax", "10");
		
		prop.put("maxCriticalBloodSugarMin", "10");
		prop.put("maxCriticalBloodSugarMax", "12");
		*/
		prop.put("CriticalBloodSugarColor", "1");
		prop.put("WarningBloodSugarColor", "5");
		prop.put("SafeBloodSugarColor", "9");
		
		
		prop.put("gameMode", "1");
		prop.store(out, "Settings file");
		out.flush();  
		out.close(); 
	}
	private static void loadProcedure() throws IOException {
		InputStream in = null;
		in =   context.getAssets().open("Settings.ini");
		prop.load(in); //loads the file contents of zones ("in" which references to the zones file) from the input stream.
		in.close();
	}
	
	
		
}
