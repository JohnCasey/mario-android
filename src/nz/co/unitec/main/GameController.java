package nz.co.unitec.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.mojang.mario.MarioComponent;
import com.mojang.mario.sprites.Mario;

public class GameController extends View implements OnTouchListener{


	public float initx = 425;
	public float inity = 267;
	
	public Point touchPoint = new Point(10,10);
	public Point pointerPosition = new Point(10,10);
	private Boolean dragging = false;
	public Bitmap originalJoyStick = BitmapFactory.decodeResource(getResources(), R.drawable.joystick);
	Bitmap joyStick = Bitmap.createScaledBitmap(originalJoyStick, (int)(originalJoyStick.getWidth() * 1.5), (int)(originalJoyStick.getHeight() * 1.5), true);
	
	Paint paint = new Paint ();

	float leftBoundary ;
	float rightBoundary;
	float topBoundary;
	float bottomBoundary;
	
	public GameController(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		init();
	}

	public GameController(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		init();
	}

	public GameController(Context context) {
		super(context);
		
		init();
	}

	
	private void init()
	{
//		setOnTouchListener(this);
		
		paint.setAntiAlias(true);
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.GRAY);
		paint.setStrokeWidth(4f);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		initx = w / 2;
		inity = h / 2;
		
		touchPoint = new Point((int)initx,(int)inity);
		pointerPosition = new Point((int)initx,(int)inity);
		
		leftBoundary= (getWidth()/2 - joyStick.getWidth()/2) + 10;
		rightBoundary = (getWidth()/2 + joyStick.getWidth()/2) - 10; 
		
		topBoundary = (getHeight()/2 - joyStick.getHeight()/2)+10;
		bottomBoundary = (getHeight()/2 + joyStick.getHeight()/2)-10;
	}
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawCircle(getWidth()/2,getHeight()/2,55, paint);		
		canvas.drawBitmap(joyStick,touchPoint.x - joyStick.getWidth()/2, touchPoint.y - joyStick.getHeight() /2, paint);
	}

	private MotionEvent lastEvent;
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
	
		int actionIndex = event.getActionIndex();
		
		if(v.getId() == R.id.gameController)
		{
			if (event == null && lastEvent == null)
			{
				return true;
			}
			else if(event == null && lastEvent != null)
			{
				event = lastEvent;
			}
			else
			{
				lastEvent = event;
			}
			 
			int action = event.getAction();
			if ( action == MotionEvent.ACTION_DOWN )
			{
				dragging = true;
			}
			else if ( action == MotionEvent.ACTION_UP)
			{
				dragging = false;
			}
			
	
			if ( dragging ){
				// get the pos
				touchPoint.x = (int)event.getX();
				touchPoint.y = (int)event.getY();
	
				// bound to a box
				if (touchPoint.x < leftBoundary){
					touchPoint.x = (int) leftBoundary;
				}
				if( touchPoint.y < topBoundary){
					touchPoint.y = (int)topBoundary;
				}
				if ( touchPoint.x > rightBoundary){
					touchPoint.x = (int)rightBoundary;
				}
				if ( touchPoint.y > bottomBoundary ){
					touchPoint.y = (int)bottomBoundary;
				}
	
				//get the angle
				double angle = Math.toDegrees(Math.atan2(touchPoint.y - inity,touchPoint.x - initx));
				
				if(angle < 0)
				{
					angle = Math.abs(angle);
				}
				else
				{
					angle = 360 - angle;
				}
	
				if(angle <60 || angle > 300)
				{
					gameView.directToggleKey(Mario.KEY_RIGHT,true);
					gameView.directToggleKey(Mario.KEY_UP,false);
					gameView.directToggleKey(Mario.KEY_LEFT,false);
					gameView.directToggleKey(Mario.KEY_DOWN,false);
				}
				
				if(angle >60 && angle < 120)
				{
					gameView.directToggleKey(Mario.KEY_RIGHT,false);
					gameView.directToggleKey(Mario.KEY_UP,true);
					gameView.directToggleKey(Mario.KEY_LEFT,false);
					gameView.directToggleKey(Mario.KEY_DOWN,false);
				}
				
				if(angle >120 && angle < 240)
				{
					gameView.directToggleKey(Mario.KEY_RIGHT,false);
					gameView.directToggleKey(Mario.KEY_UP,false);
					gameView.directToggleKey(Mario.KEY_LEFT,true);
					gameView.directToggleKey(Mario.KEY_DOWN,false);
				}
				
				if(angle >240 && angle < 300)
				{
					gameView.directToggleKey(Mario.KEY_RIGHT,false);
					gameView.directToggleKey(Mario.KEY_UP,false);
					gameView.directToggleKey(Mario.KEY_LEFT,false);
					gameView.directToggleKey(Mario.KEY_DOWN,true);
	
				}
	
	
			}
			else if (!dragging)
			{
				// Snap back to center when the joystick is released
				touchPoint.x = (int) initx;
				touchPoint.y = (int) inity;
				
				gameView.directToggleKey(Mario.KEY_RIGHT,false);
				gameView.directToggleKey(Mario.KEY_UP,false);
				gameView.directToggleKey(Mario.KEY_LEFT,false);
				gameView.directToggleKey(Mario.KEY_DOWN,false);
				//shaft.alpha = 0;
			}
			postInvalidate();
			return true;
		}
		else
		{
			return false;
		}
	}

	MarioComponent gameView;
	public void setMarioComponent(MarioComponent gameView) {
		this.gameView = gameView;
	}
	
	
}
