package nz.co.unitec.main;

import java.util.ArrayList;

import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class MultitouchButtonHandler {
	public class MultitouchButtonHandlerResult {
	    public View view;
	    public int event_type;
	}
    ArrayList<View> views_info = new ArrayList<View>();
    public MultitouchButtonHandlerResult onTouchEvent(View v, MotionEvent ev) {

    //GET EVENT INFO 
    final int action = ev.getAction();
    int action_masked = action & MotionEvent.ACTION_MASK;

        //GET ABSOLUTE SIZE AND CHECK IF THIS ANY VIEW ATTACHED TO THIS POSITION
        final int original_location[] = { 0, 0 };
        v.getLocationOnScreen(original_location);
        
        final int actionPointerIndex = ev.getActionIndex();
        
        float rawX = (int) ev.getX(actionPointerIndex) + original_location[0];
        float rawY = (int) ev.getY(actionPointerIndex) + original_location[1];
        
        View eventView = getViewByLocation((int)rawX, (int)rawY);
        if(eventView==null) return null;

        MultitouchButtonHandlerResult result = new MultitouchButtonHandlerResult();
        result.view  = eventView;


        //CHECK WHAT KIND OF EVENT HAPPENED 
        switch (action_masked) {
        case MotionEvent.ACTION_DOWN: {
            result.event_type = MotionEvent.ACTION_DOWN;
            return result;
        }

        case MotionEvent.ACTION_UP: {
            result.event_type = MotionEvent.ACTION_UP;
            return result;
        }

        case MotionEvent.ACTION_CANCEL: {
            result.event_type = MotionEvent.ACTION_CANCEL;
            return result;
        }

        case MotionEvent.ACTION_POINTER_UP: {
            result.event_type = MotionEvent.ACTION_UP;
            return result;
        }

        case MotionEvent.ACTION_POINTER_DOWN: {
            result.event_type = MotionEvent.ACTION_DOWN;
            return result;
        }

        default:

        break;

        }

        return null;
    }

    public void addMultiTouchView(View v){
        views_info.add(v);;
    }
    public void removeMultiTouchView(View v){
        views_info.remove(v);;
    }

    public View getViewByLocation(int x, int y){

        for(int key=0; key!= views_info.size(); key++){
            View v = views_info.get(key);
            //GET BUTTON ABSOLUTE POSITION ON SCREEN
            int[] v_location = { 0, 0 };
            v.getLocationOnScreen(v_location);

            //FIND THE BOUNDS
            Point min = new Point(v_location[0], v_location[1]);
            Point max = new Point(min.x + v.getWidth(), min.y + v.getHeight());


            if(x>=min.x && x<=max.x && y>=min.y && y<=max.y){
                //Log.d("mylog", "***Found a view: " + v.getId());
                return v;
            }

        }

        //Log.d("mylog", "Searching: " + x +", " + y + " but not found!");

        return null;
    }

}
