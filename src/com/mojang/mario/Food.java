package com.mojang.mario;

import android.graphics.Bitmap;


public class Food {

	public String foodName;
	public float bloodSugarAmount;
	public boolean enabled = true;
	int color = 3;
	public Bitmap Icon = null;
	public Food(String foodName, float bloodSugarArmount) {
		this.foodName = foodName;
		this.bloodSugarAmount = bloodSugarArmount;
	}
	public Food(String foodName, float bloodSugarArmount, String iconFileName) {
		// TODO Auto-generated constructor stub
		this.foodName = foodName;
		this.bloodSugarAmount = bloodSugarArmount;
	}

}
