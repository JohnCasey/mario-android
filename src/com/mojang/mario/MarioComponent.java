package com.mojang.mario;

import java.util.ArrayList;
import java.util.Random;

import nz.co.unitec.main.AndroidMarioActivity;
import nz.co.unitec.main.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.mojang.mario.level.LevelGenerator;
import com.mojang.mario.sprites.Mario;
import com.mojang.sonar.AndroidSoundEngine;

public class MarioComponent extends SurfaceView implements Runnable,
		SurfaceHolder.Callback {
	SurfaceHolder surfaceHolder;
	public static final int TICKS_PER_SECOND = 24;
	private Thread animation;
	private boolean running = false;
	private int width = 0, height = 0;
	private boolean isBoy;

	ArrayList <View> controls = new ArrayList<View>();
	AndroidMarioActivity activity;
	
	public AndroidMarioActivity getActivity() {
		return activity;
	}

	public void setActivity(AndroidMarioActivity activity) {
		this.activity = activity;
	}

	public ArrayList<View> getControls() {
		return controls;
	}

	public void setControls(ArrayList<View> controls) {
		this.controls = controls;
	}

	public void setBoy(boolean isBoy) {
		this.isBoy = isBoy;
		Start();
	}

	private Scene scene;

	/*
	 * private boolean focused = false; private boolean useScale2x = false;
	 */
	private MapScene mapScene;
	private boolean surfaceChanged;

	public MarioComponent(Context context, boolean isBoy) {
		super(context);
		this.isBoy = isBoy;
		surfaceHolder = getHolder();
		
	}

	public MarioComponent(Context context, AttributeSet attrs) {
		super(context, attrs);
		surfaceHolder = getHolder();

	}

	public void Start() {
		AndroidSoundEngine.setContex(getContext());
		animation = new Thread(this,"Animation");
		running = true;
		animation.start();
	}

	public void directToggleKey(int keyCode, boolean isPressed) {
		scene.toggleKey(keyCode, isPressed);
	}
	
	public void toggleKey(int keyCode, boolean isPressed) {
		if (keyCode == R.id.btn_run_fire) {
			scene.toggleKey(Mario.KEY_SPEED, isPressed);
		}
		if (keyCode == R.id.btn_select) {
			scene.toggleKey(Mario.KEY_MENU, isPressed);
			scene.toggleKey(Mario.KEY_ENTER, isPressed);
			scene.toggleKey(Mario.KEY_CONFIRM, isPressed);
		}
		/*
		 * if (keyCode == KeyEvent.VK_W || keyCode == KeyEvent.VK_ENTER) {
		 * scene.toggleKey(Mario.KEY_CONFIRM, isPressed); }
		 */

		if (keyCode == R.id.btn_jump) {
			scene.toggleKey(Mario.KEY_JUMP, isPressed);
		}/*
		 * if (keyCode == KeyEvent.VK_ESCAPE) { scene.toggleKey(Mario.KEY_QUIT,
		 * isPressed); } if (isPressed && keyCode == KeyEvent.VK_F1) {
		 * useScale2x = !useScale2x; }
		 */
	}

	public void stop() {
		CSVLogger.LogEnd(Scene.name, Scene.sessionNum, 4, "Game End(Quit)");
	//	CSVLogger.CloseWriter();
		Art.stopMusic();
		running = false;
	}

	public void resume() {
		this.levelFailed();
	}

	
	public void pause() {
		Art.stopMusic();
	}

	final Paint paint = new Paint();
	public void run() {
		// scene = new LevelScene(this, height, height, height);
		mapScene = new MapScene(this, new Random().nextLong());

		scene = mapScene;

		Art.init(getContext(), isBoy);

		int lastTick = -1;
		// double lastNow = 0;
		int renderedFrames = 0;
		int fps = 0;

		// double now = 0;
		// double startTime = System.nanoTime() / 1000000000.0;
		// double timePerFrame = 0;
		double time = System.nanoTime() / 1000000000.0;
		double now = time;
		double averagePassedTime = 0;

		boolean naiveTiming = true;

		toTitle();
//		startGame();
		
		//startLevel(0,0,LevelGenerator.TYPE_OVERGROUND);

		while (running) {

			double lastTime = time;
			time = System.nanoTime() / 1000000000.0;
			double passedTime = time - lastTime;

			if (passedTime < 0)
				naiveTiming = false; // Stop relying on nanotime if it starts
										// skipping around in time (ie running
										// backwards at least once). This
										// sometimes happens on dual core amds.
			averagePassedTime = averagePassedTime * 0.9 + passedTime * 0.1;

			if (naiveTiming) {
				now = time;
			} else {
				now += averagePassedTime;
			}

			int tick = (int) (now * TICKS_PER_SECOND);
			if (lastTick == -1)
				lastTick = tick;
			while (lastTick < tick) {
				scene.tick();
				lastTick++;

				if (lastTick % TICKS_PER_SECOND == 0) {
					fps = renderedFrames;
					renderedFrames = 0;
				}
			}

			float alpha = (float) (now * TICKS_PER_SECOND - tick);

			Canvas canvas = null;

			if (surfaceChanged) {
				surfaceHolder = getHolder();
				surfaceChanged = false;
			}
			synchronized (surfaceHolder) {

				canvas = surfaceHolder.lockCanvas();
				if (canvas == null) {
					surfaceChanged = true;
				} else
				{
					canvas.scale((float)width / 320f, (float)height / 240f); // scale them up a bit
					canvas.drawColor(Color.WHITE);

					scene.render(canvas, alpha, paint);
				}
			}
			if (canvas != null)
				surfaceHolder.unlockCanvasAndPost(canvas);

			renderedFrames++;

			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
			}
		}

		Art.stopMusic();
	}

	private void drawString(Canvas g, String text, int x, int y, int c) {
		char[] ch = text.toCharArray();
		for (int i = 0; i < ch.length; i++) {
			g.drawBitmap(Art.font[ch[i] - 32][c], x + i * 8, y, null);
		}
	}

	public void startLevel(long seed, int difficulty, int type) {
		scene = new LevelScene(this, seed, difficulty, type);

		scene.init();
	}

	public void levelFailed() {
		scene = mapScene;
		mapScene.startMusic();
		Mario.lives--;
		if (Mario.lives == 0) {
			lose();
		}
	}

	public void levelWon() {
		scene = mapScene;
		mapScene.startMusic();
		mapScene.levelWon();
	}

	public void win() {
		scene = new WinScene(this);
		scene.init();
	}

	public void toTitle() {
		// if (Scene.sessionNum != 0)
		// {
		// mapScene.
		//
		// }
		Mario.resetStatic();
		scene = new TitleScene(this);
		scene.init();
	}

	public void lose() {
		scene = new LoseScene(this);
		scene.init();
	}

	public void startGame() {
		scene = mapScene;
		mapScene.startMusic();
		mapScene.init();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		this.width = w;
		this.height = h;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		surfaceHolder = getHolder();
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		surfaceHolder = getHolder();

	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		surfaceHolder = getHolder();
	}

	public int getMyHeight() {
		return height;
	}

	public void setMyHeight(int height) {
		this.height = height;
	}

	public int getMyWidth() {
		return width;
	}

	public void setMyWidth(int width) {
		this.width = width;
	}

	public void setFilePath(String filePath) {

		Scene.filePath = filePath;
	}

	public void setUserName(String userName) {
		Scene.name = userName;
		// :P
	}
}