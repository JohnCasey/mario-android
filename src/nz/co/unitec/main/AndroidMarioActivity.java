package nz.co.unitec.main;

import java.io.File;

import nz.co.unitec.main.MultitouchButtonHandler.MultitouchButtonHandlerResult;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.mojang.mario.CSVLogger;
import com.mojang.mario.INIManager;
import com.mojang.mario.MarioComponent;
import com.mojang.mario.sprites.Mario;

public class AndroidMarioActivity extends Activity implements OnTouchListener {
	/** Called when the activity is first created. */
	// /////////////////////////////////User Selections
	private String userName;
	private boolean isMale;
	
	private EditText et_userName;
	private Button btn_start;
	private RadioButton isBoy;
	// ///////////////////////////////Mario Game Components
	MarioComponent gameView;
	Button btn_left, btn_right, btn_jump, btn_down, btn_run_fire, btn_select;

	private MultitouchButtonHandler multitouch_handler = new MultitouchButtonHandler();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		INIManager.context = this;
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.user_selection_vew_layout);

		et_userName = (EditText) findViewById(R.id.et_log);
		isBoy = (RadioButton) findViewById(R.id.rb_boy);
		isBoy.setChecked(true);
		btn_start = (Button) findViewById(R.id.btn_start);
		btn_start.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				if (et_userName != null) {
					if (!et_userName.getText().toString().isEmpty()) {
						userName = et_userName.getText().toString();
						initializeGameView(isBoy.isChecked());
					}
				}
				return true;
			}
		});
		
		userName = "Mario";
		
		initializeGameView(true);
	}

	GameController controller;
	
	private void initializeGameView(boolean isBoy) {
		setContentView(R.layout.main);

		gameView = (MarioComponent) findViewById(R.id.marioComponent1);
		gameView.setUserName(userName);
		gameView.setFilePath(getFilesDir()+File.separator);
		System.out.println(getFilesDir());
		et_userName = (EditText) findViewById(R.id.et_log);
		btn_start = (Button) findViewById(R.id.btn_start);

		gameView.setActivity(this);
		
		controller = (GameController)findViewById(R.id.gameController);
		controller.setMarioComponent(gameView);
		btn_jump = (Button) findViewById(R.id.btn_jump);
		btn_run_fire = (Button) findViewById(R.id.btn_run_fire);
		btn_select = (Button) findViewById(R.id.btn_select);

		multitouch_handler.addMultiTouchView(controller);
		multitouch_handler.addMultiTouchView(btn_jump);
//		multitouch_handler.addMultiTouchView(btn_run_fire);
//		multitouch_handler.addMultiTouchView(btn_select);

		gameView.setOnTouchListener(this);

		controller.setOnTouchListener(this);
		btn_jump.setOnTouchListener(this);
		btn_run_fire.setOnTouchListener(this);
		btn_select.setOnTouchListener(this);
		gameView.setBoy(isBoy);
		
		gameView.getControls().add(controller);
		gameView.getControls().add(btn_jump);
//		gameView.getControls().add(btn_run_fire);
//		gameView.getControls().add(btn_select);		
	}

	@Override
	protected void onPause() {
		super.onPause();
		try {
			if (gameView != null)
			gameView.pause();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		try {
			if (gameView != null)
			gameView.resume();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.gameController)
		{
			controller.onTouch(v,event);
		}
		
		// jcasey 13/05/2014 disable the food items menu.
		if(v.getId() == R.id.marioComponent1)
		{
			if(event.getAction() == MotionEvent.ACTION_DOWN)
			{
//				gameView.directToggleKey(Mario.KEY_MENU, true);
				gameView.directToggleKey(Mario.KEY_ENTER, true);
//				gameView.directToggleKey(Mario.KEY_CONFIRM, true);
			}
			else
			{
//				gameView.directToggleKey(Mario.KEY_MENU, false);
				gameView.directToggleKey(Mario.KEY_ENTER, false);
//				gameView.directToggleKey(Mario.KEY_CONFIRM, false);
			}
		}
		
		MultitouchButtonHandlerResult result = multitouch_handler.onTouchEvent(v, event);
		if (result == null)
		{
			return true;
		}

		
		switch (result.event_type) {
		case MotionEvent.ACTION_DOWN:
			
			if(result.view instanceof Button)
			{
				Button state = (Button) result.view;
				state.setPressed(true);
			}
			
			gameView.toggleKey(result.view.getId(), true);
			break;
		case MotionEvent.ACTION_UP:
			
			if(result.view instanceof Button)
			{
				Button state = (Button) result.view;
				state.setPressed(false);
			}
			gameView.toggleKey(result.view.getId(), false);
			break;
		case MotionEvent.ACTION_CANCEL:
			break;

		}
		return true;
		/*
		 * if(event.getAction() == MotionEvent.ACTION_DOWN)
		 * gameView.toggleKey(v.getId(), true); else if (event.getAction() ==
		 * MotionEvent.ACTION_UP) gameView.toggleKey(v.getId(), false);
		 */

	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.emailRecords:
	            emailRecords();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	private void emailRecords() {
		Intent intent = new Intent(Intent.ACTION_SEND); // it's not ACTION_SEND
		/*intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
		intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
		//intent.setData(Uri.parse("mailto:default@recipient.com")); // or just "mailto:" for blank
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
		*/
		intent.setAction(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"john.casey@gmail.com"});
		intent.putExtra(Intent.EXTRA_SUBJECT,"subject");
		intent.putExtra(Intent.EXTRA_TEXT, "mail content");
		
		File file = new File(CSVLogger.getLogFileName());
		Uri fileUri =  Uri.fromFile(file);
		intent.putExtra(Intent.EXTRA_STREAM, fileUri);
		System.out.println(fileUri.toString());
		
		System.out.println("Emailed Records");
		CSVLogger.LogEnd(userName, -1, -1, "Sending Records");
		CSVLogger.CloseWriter();
		startActivity(intent);
		
	}
}