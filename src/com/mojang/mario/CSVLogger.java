package com.mojang.mario;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;

import android.text.format.Time;

public class CSVLogger {
	final static Time now = new Time();

	static boolean WriterCreated = false;
	static OutputStreamWriter writer;
	static String LogFile = "Log.csv";

	private static void CreateWriter() {

		LogFile = "Log " + getSimpleDateNow() + ".csv";
		try {
			File LogFolder = new File("/sdcard/" + "Logs/" );

			if (!LogFolder.exists())
				LogFolder.mkdirs();
			File logFile = new File("/sdcard/" + "Logs/" + LogFile);
			System.out.println("Created Writer Path: " + "/sdcard/" + "Logs/" + LogFile);
			// File logFile = new File("/sdcard/" +LogFile);
			logFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(logFile);
			writer = new OutputStreamWriter(fOut);
			WriterCreated = true;
			// FileWriter fstream = new FileWriter(LogFile, true);
			// writer = new BufferedWriter(fstream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private static String getSimpleDateNow() {
		now.setToNow();
		
		return now.format("%Y%m%d%H%M%S");
	}
	
	private static String getDateNow() {
		now.setToNow();
		
		return now.format3339(false);
	}

	public static void CloseWriter() {
		if (writer != null) {
			try {
				writer.flush();
				writer.close();
				WriterCreated = false;
				System.out.println("Closed Writer");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void LogHeader() {
		System.out.println("Logged header");
		Log("");
		LogNewLine();
		LogNewLine();
		Log("ID");
		Log("Description");
		Log("User ID");
		Log("Session Number");
		Log("Time");

		Log("Blood Sugar Level");
		Log("Menu Chosen");

		Log("Food Eaten");
		Log("Insulin");
		Log("Blood Sugar Level After");

		Log("Exit Reasion ID");
		Log("Exit Reasion");

		Log("Feedback Message");

		try {
			writer.write("\r\n");
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block e.printStackTrace(); } try {
			LogNewLine();
		}

	}

	public static void LogStart(String userID, int sessionNumber) {
		LogHeader();

		Log("1");
		Log("Start Of Session");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		LogNewLine();

	}

	/*
	 * public static void LogForceMenu(String userID,int sessionNumber) {
	 * Log("2"); Log(userID); Log(sessionNumber); Log(GetDateNow("HH:mm")); try
	 * { writer.write("\r\n"); writer.flush(); } catch (IOException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } }
	 */
	public static void LogUserSelectedMenu(String userID, int sessionNumber,
			float bloodSugarLevel, String menu) {
		Log("3");
		Log("User Selected Menu");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		Log(bloodSugarLevel);
		Log(menu);
		LogNewLine();

	}

	public static void LogFoodChosen(String userID, int sessionNumber,
			String food, float bloodSugarLevel, float bloodSugarLevelAfter) {
		Log("4");
		Log("Food Chosen");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		Log(bloodSugarLevel);
		Log("");
		Log(food);
		Log("");
		Log(bloodSugarLevelAfter);
		LogNewLine();

	}

	public static void LogInsulinTaken(String userID, int sessionNumber,
			int dose, float bloodSugarLevel, float bloodSugarLevelAfter) {

		Log("5");
		Log("Insulin Taken");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		Log(bloodSugarLevel);
		Log("");
		Log("");
		Log(dose);
		Log(bloodSugarLevelAfter);

		LogNewLine();

	}

	public static void LogWarning(String userID, int sessionNumber,
			float bloodSugarLevel) {
		Log("6");
		Log("Blood Sugar Warning");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		Log(bloodSugarLevel);
		LogNewLine();

	}

	public static void LogCritical(String userID, int sessionNumber,
			float bloodSugarLevel) {

		Log("7");
		Log("Blood Sugar Critical");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		Log(bloodSugarLevel);
		LogNewLine();

	}

	public static void LogFeedbackGiven(String userID, int sessionNumber,
			float bloodSugarLevel, String feebackText) {

		Log("7");
		Log("FeedbackGiven");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		Log(bloodSugarLevel);
		Log("");
		Log("");
		Log("");
		Log("");
		Log("");
		Log("");
		Log(feebackText);
		LogNewLine();

	}

	public static void LogEnd(String userID, int sessionNumber, int reason,
			String reasonString) {

		Log("8");
		Log("End of Game or session");
		Log(userID);
		Log(sessionNumber);
		Log(getDateNow());
		Log("");
		Log("");
		Log("");
		Log("");
		Log("");
		Log(reason);
		Log(reasonString);

		LogNewLine();
	}

	private static void LogNewLine() {
		if (!WriterCreated)
			CreateWriter();
		try {
			writer.write("\r\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void Log(String value) {
		if (!WriterCreated)
			CreateWriter();

		try {
			writer.write(value);
			writer.write(",");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void Log(int value) {
		if (!WriterCreated)
			CreateWriter();

		try {
			writer.write(Integer.toString(value));
			writer.write(",");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void Log(float value) {
		if (!WriterCreated)
			CreateWriter();

		try {
			writer.write(Float.toString(value));
			writer.write(",");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getLogFileName() {
		if (!WriterCreated)
			CreateWriter();
System.out.println("/sdcard/" + "Logs/" + LogFile);
		return "/sdcard/" + "Logs/" + LogFile;
	}

	/*
	 * 
	 * � Type 1: start of session - write an entry for message type 1 to the log
	 * with: userid, session number??, record type, start time of session
	 * 
	 * � Type 2: force menu - write an entry to the log each time a menu is
	 * forced with: userid, session number, record type, time menu forced
	 * 
	 * � Type 3: user selected menu - write an entry to the log each time a menu
	 * is chosen by the user with: userid, session number, record type, time
	 * menu chosen, blood sugar level.
	 * 
	 * � Type 4: food chosen - write an entry when a food type is chosen with:
	 * userid, session number, record type, food chosen, blood sugar level, time
	 * 
	 * � Type 5: insulin taken - write an entry when insulin is taken with:
	 * userid, session number, record type, dose, blood sugar level, time
	 * 
	 * � Type 6: session end: userid, session number, record type, reason for
	 * session end (timeout=1 or game over=2, quit = 3), end session time.
	 * 
	 * 
	 * 
	 * private void loadData() { File folderExisting = new File("C:\\folder1");
	 * 
	 * if (!folderExisting.exists()) { folderExisting.mkdir(); } File foodsFile
	 * = new File("C:\\Temp\\Foods.txt"); //StringBuffer foodsContents = new
	 * StringBuffer(); BufferedReader foodReader = null;
	 * 
	 * try { foodReader = new BufferedReader(new FileReader(foodsFile)); String
	 * text = null;
	 * 
	 * // repeat until all lines is read int foodNum = 0; foods = new
	 * Food[Integer.parseInt(foodReader.readLine())]; while ((text =
	 * foodReader.readLine()) != null) { foods[foodNum] = new Food(text,
	 * Float.parseFloat(foodReader .readLine())); foods[foodNum].icon =
	 * foodReader.readLine().charAt(0); foodNum++; } } catch
	 * (FileNotFoundException e) { try { // Create file FileWriter fstream = new
	 * FileWriter("C:\\Temp\\Foods.txt"); BufferedWriter out = new
	 * BufferedWriter(fstream); out.write("5\r\n"); out.write("Sandwich\r\n");
	 * out.write("1f\r\n"); out.write("!\r\n"); out.write("Juice\r\n");
	 * out.write("1.9f\r\n"); out.write("@\r\n"); out.write("Apple\r\n");
	 * out.write("2.5f\r\n"); out.write("#\r\n"); out.write("Pie\r\n");
	 * out.write("3.4f\r\n"); out.write("$\r\n"); out.write("Chicken\r\n");
	 * out.write("5f\r\n"); out.write("%\r\n"); // Close the output stream
	 * out.close(); loadData(); } catch (Exception e2) {// Catch exception if
	 * any System.err.println("Error: " + e2.getMessage()); } } catch
	 * (IOException e) { e.printStackTrace(); } finally { try { if (foodReader
	 * != null) { foodReader.close(); } } catch (IOException e) {
	 * e.printStackTrace(); } }
	 * 
	 * 
	 * File bsDropValuesFile = new File("C:\\Temp\\BloodSugarValues.txt");
	 * //StringBuffer bsDropContents = new StringBuffer(); BufferedReader
	 * bsReaderReader = null;
	 * 
	 * try { bsReaderReader = new BufferedReader(new
	 * FileReader(bsDropValuesFile)); String text = null;
	 * 
	 * // repeat until all lines is read
	 * 
	 * while ((text = bsReaderReader.readLine()) != null) { if
	 * (Mario.loadedBloodSugar == false) { Mario.bloodSugar =
	 * Float.parseFloat(text); Mario.loadedBloodSugar = true; }
	 * idleBloodSugarDrop = Float.parseFloat(bsReaderReader.readLine());
	 * walkingBloodSugarDrop =Float.parseFloat(bsReaderReader.readLine());
	 * runningBloodSugarDrop = Float.parseFloat(bsReaderReader.readLine());
	 * jumpBloodSugarDrop = Float.parseFloat(bsReaderReader.readLine());
	 * minBloodSugar = Float.parseFloat(bsReaderReader.readLine());
	 * maxBloodSugar = Float.parseFloat(bsReaderReader.readLine()); }
	 * 
	 * } catch (FileNotFoundException e) { try { // Create file FileWriter
	 * fstream = new FileWriter("C:\\Temp\\BloodSugarValues.txt");
	 * BufferedWriter out = new BufferedWriter(fstream); out.write("6.4f\r\n");
	 * out.write(idleBloodSugarDrop + "f\r\n"); out.write(walkingBloodSugarDrop
	 * + "f\r\n"); out.write( runningBloodSugarDrop+"f\r\n");
	 * out.write(jumpBloodSugarDrop + "f\r\n"); out.write(minBloodSugar
	 * +"\r\n"); out.write(maxBloodSugar+"\r\n"); // Close the output stream
	 * out.close(); } catch (Exception e2) {// Catch exception if any
	 * System.err.println("Error: " + e2.getMessage()); } } catch (IOException
	 * e) { e.printStackTrace(); } finally { try { if (bsReaderReader != null) {
	 * bsReaderReader.close(); } } catch (IOException e) { e.printStackTrace();
	 * } } }
	 */

}
