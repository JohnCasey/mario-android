package com.mojang.sonar;

import java.util.HashSet;

import android.content.Context;
import android.media.MediaPlayer;

public class AndroidSoundEngine {

	private static HashSet<MediaPlayer> mpSet = new HashSet<MediaPlayer>();
	static MediaPlayer songMP;
	private static Context context;

	// private MediaPlayer mp;
	public static void setContex(Context context2) {
		context = context2;
		// mp = new MediaPlayer();
	}

	public static void play(int resId) {
		MediaPlayer mp = MediaPlayer.create(context, resId);
		mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				mpSet.remove(mp);
				mp.stop();
				mp.release();
			}
		});
		mpSet.add(mp);
		mp.start();
	}

	public static void playSong(int song) {
		if (songMP == null) {
			songMP = MediaPlayer.create(context, song);

			songMP.setLooping(true);
			songMP.start();
		} else {
			 stopSong();
			 playSong(song);
		}
	}

	public static void stopSong() {
		 if (songMP != null) {
		 songMP.stop();
		 songMP.release();
		 songMP = null;

		 }

	}
}
