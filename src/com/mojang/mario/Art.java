package com.mojang.mario;
/*
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
*/
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import nz.co.unitec.main.R;
/*
import javax.imageio.ImageIO;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
*/
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.mojang.sonar.AndroidSoundEngine;
import com.mojang.sonar.SonarSoundEngine;
import com.mojang.sonar.sample.SonarSample;


public class Art
{
    public static final int SAMPLE_BREAK_BLOCK = 0;
    public static final int SAMPLE_GET_COIN = 1;
    public static final int SAMPLE_MARIO_JUMP = 2;
    public static final int SAMPLE_MARIO_STOMP = 3;
    public static final int SAMPLE_MARIO_KICK = 4;
    public static final int SAMPLE_MARIO_POWER_UP = 5;
    public static final int SAMPLE_MARIO_POWER_DOWN = 6;
    public static final int SAMPLE_MARIO_DEATH = 7;
    public static final int SAMPLE_ITEM_SPROUT = 8;
    public static final int SAMPLE_CANNON_FIRE = 9;
    public static final int SAMPLE_SHELL_BUMP = 10;
    public static final int SAMPLE_LEVEL_EXIT = 11;
    public static final int SAMPLE_MARIO_1UP = 12;
    public static final int SAMPLE_MARIO_FIREBALL = 13;

    public static Bitmap[][] mario;
    public static Bitmap[][] smallMario;
    public static Bitmap[][] fireMario;
    public static Bitmap[][] enemies;
    public static Bitmap[][] items;
    public static Bitmap[][] level;
    public static Bitmap[][] particles;
    public static Bitmap[][] font;
    public static Bitmap[][] bg;
    public static Bitmap[][] map;
    public static Bitmap[][] endScene;
    public static Bitmap[][] gameOver;
    public static Bitmap logo;
    public static Bitmap titleScreen;

    public static SonarSample[] samples = new SonarSample[100];

  //  private static Sequence[] songs = new Sequence[10];
  //  private static Sequencer sequencer;

    public static void init(Context res, boolean isBoy)
    {
        try
        {
    
           /* mario = cutImage(res, "mariosheet.png", 32, 32);
            smallMario = cutImage(res, "smallmariosheet.png", 16, 16);
            fireMario = cutImage(res, "firemariosheet.png", 32, 32);
            enemies = cutImage(res, "enemysheet.png", 16, 32);
            items = cutImage(res, "itemsheet.png", 16, 16);
            level = cutImage(res, "mapsheet.png", 16, 16);
            map = cutImage(res, "worldmap.png", 16, 16);
            particles = cutImage(res, "particlesheet.png", 8, 8);
            bg = cutImage(res, "bgsheet.png", 32, 32);
            logo = getImage(res, "logo.gif");
            titleScreen = getImage(res, "title.gif");
            font = cutImage(res, "font.gif", 8, 8);
            endScene = cutImage(res, "endscene.gif", 96, 96);
            gameOver = cutImage(res, "gameovergost.gif", 96, 64);*/
        	if(isBoy)
        	{
        	 mario = cutImage(res, R.drawable.mariosheet, 32, 32);
             smallMario = cutImage(res, R.drawable.smallmariosheet, 16, 16);
             fireMario = cutImage(res, R.drawable.firemariosheet, 32, 32);
             map = cutImage(res, R.drawable.worldmap, 16, 16);
        	}
        	else
        	{
        		mario = cutImage(res, R.drawable.girlsheet, 32, 32);
                smallMario = cutImage(res, R.drawable.smallgirlsheet, 16, 16);
                fireMario = cutImage(res, R.drawable.firegirlsheet, 32, 32);
                map = cutImage(res, R.drawable.worldmapgirl, 16, 16);
        	}
             enemies = cutImage(res, R.drawable.enemysheet, 16, 32);
             items = cutImage(res, R.drawable.itemsheet, 16, 16);
             level = cutImage(res, R.drawable.mapsheet, 16, 16);
             
             particles = cutImage(res, R.drawable.particlesheet, 8, 8);
             bg = cutImage(res, R.drawable.bgsheet, 32, 32);
             logo = getImage(res, R.drawable.logo);
             titleScreen = getImage(res, R.drawable.title);
             font = cutImage(res, R.drawable.font, 8, 8);
             endScene = cutImage(res, R.drawable.endscene, 96, 96);
             gameOver = cutImage(res, R.drawable.gameovergost, 96, 64);

            /*if (sound != null)
            {
                samples[SAMPLE_BREAK_BLOCK] = sound.loadSample("snd/breakblock.wav");
                samples[SAMPLE_GET_COIN] = sound.loadSample("snd/coin.wav");
                samples[SAMPLE_MARIO_JUMP] = sound.loadSample("snd/jump.wav");
                samples[SAMPLE_MARIO_STOMP] = sound.loadSample("snd/stomp.wav");
                samples[SAMPLE_MARIO_KICK] = sound.loadSample("snd/kick.wav");
                samples[SAMPLE_MARIO_POWER_UP] = sound.loadSample("snd/powerup.wav");
                samples[SAMPLE_MARIO_POWER_DOWN] = sound.loadSample("snd/powerdown.wav");
                samples[SAMPLE_MARIO_DEATH] = sound.loadSample("snd/death.wav");
                samples[SAMPLE_ITEM_SPROUT] = sound.loadSample("snd/sprout.wav");
                samples[SAMPLE_CANNON_FIRE] = sound.loadSample("snd/cannon.wav");
                samples[SAMPLE_SHELL_BUMP] = sound.loadSample("snd/bump.wav");
                samples[SAMPLE_LEVEL_EXIT] = sound.loadSample("snd/exit.wav");
                samples[SAMPLE_MARIO_1UP] = sound.loadSample("snd/1-up.wav");
                samples[SAMPLE_MARIO_FIREBALL] = sound.loadSample("snd/fireball.wav");
            }*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
        	/*
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            songs[0] = MidiSystem.getSequence(Art.class.getResourceAsStream("mus/smb3map1.mid"));
            songs[1] = MidiSystem.getSequence(Art.class.getResourceAsStream("mus/smwovr1.mid"));
            songs[2] = MidiSystem.getSequence(Art.class.getResourceAsStream("mus/smb3undr.mid"));
            songs[3] = MidiSystem.getSequence(Art.class.getResourceAsStream("mus/smwfortress.mid"));
            songs[4] = MidiSystem.getSequence(Art.class.getResourceAsStream("mus/smwtitle.mid"));
            */
        }
        catch (Exception e)
        {
            //sequencer = null;
            e.printStackTrace();
        }
    }

    public static Bitmap getImage(Context context, int id) throws IOException
    {
    	
    	InputStream is = context.getResources().openRawResource(id);
    	Bitmap originalBitmap = BitmapFactory.decodeStream(is);  
    	
        return originalBitmap;
    }
    
    private static Bitmap[][] cutImage(Context context, int id, int xSize, int ySize) throws IOException
    {
    	
        Bitmap source = getImage(context, id);
        Bitmap[][] images = new Bitmap[source.getWidth() / xSize][source.getHeight() / ySize];
        for (int x = 0; x < source.getWidth() / xSize; x++)
        {
            for (int y = 0; y < source.getHeight() / ySize; y++)
            {
            	//Bitmap image = gc.createCompatibleImage(xSize, ySize, Transparency.BITMASK);
            	//Bitmap image = Bitmap.createBitmap(xSize, ySize, null);
            	//Bitmap image2 = Bitmap.createBitmap(source, x, y, xSize, ySize)
            	
            	
               // Graphics2D g = (Graphics2D) image.getGraphics();
               // g.setComposite(AlphaComposite.Src);
               // g.drawImage(source, -x * xSize, -y * ySize, null);
              //  g.dispose();
                Bitmap image = Bitmap.createBitmap(xSize, ySize,Config.ARGB_8888);
                Canvas c = new Canvas(image);
            	
            	c.drawBitmap(source, -x * xSize, -y * ySize, null);
                images[x][y] = image;//Bitmap.createBitmap(source, x * xSize, y * ySize, xSize, ySize);
            }
        }

        return images;
    }

    public static void startMusic(int song)
    {
    	AndroidSoundEngine.playSong(song);
    	/*
        stopMusic();
        if (sequencer != null)
        {
            try
            {
                sequencer.open();
                sequencer.setSequence((Sequence)null);
                sequencer.setSequence(songs[song]);
                sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
                sequencer.start();
            }
            catch (Exception e)
            {
            }
        }
        */
    }

    public static void stopMusic()
    {
    	AndroidSoundEngine.stopSong();
    	/*
        if (sequencer != null)
        {
            try
            {
                sequencer.stop();
                sequencer.close();
            }
            catch (Exception e)
            {
            }
        }
        */
    }
}