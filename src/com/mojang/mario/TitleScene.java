package com.mojang.mario;

//import java.awt.Graphics;
//import java.awt.GraphicsConfiguration;

import nz.co.unitec.main.R;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.mojang.mario.level.BgLevelGenerator;
import com.mojang.mario.level.LevelGenerator;
import com.mojang.mario.sprites.Mario;

public class TitleScene extends Scene
{
    private MarioComponent component;
    private int tick;
    private BgRenderer bgLayer0;
    private BgRenderer bgLayer1;
    
    public TitleScene(MarioComponent component)
    {
        this.component = component;
        bgLayer0 = new BgRenderer(BgLevelGenerator.createLevel(2048, 15, false, LevelGenerator.TYPE_OVERGROUND),component.getMyWidth()  , component.getMyHeight(), 1);        
        bgLayer1 = new BgRenderer(BgLevelGenerator.createLevel(2048, 15, true, LevelGenerator.TYPE_OVERGROUND),  component.getMyWidth()  , component.getMyHeight(), 2);
    }

    public void init()
    {
    	component.getActivity().runOnUiThread(new Runnable(){
    		public void run(){
		    	for(View control: component.getControls())
		    	{
		    		control.setVisibility(View.INVISIBLE);
		     		control.setEnabled(false);
		    	}
    		}
    	});

    	
        Art.startMusic(R.raw.smwtitle);

    }

    public void render(Canvas g, float alpha, Paint paint)
    {
    	
        bgLayer0.setCam(tick+160, 0);
        bgLayer1.setCam(tick+160, 0);
        bgLayer1.render(g, tick, alpha, paint);
        bgLayer0.render(g, tick, alpha, paint);
//        g.setColor(Color.decode("#8080a0"));
//        g.fillRect(0, 0, 320, 240);
        //int yo = 16-Math.abs((int)(Math.sin((tick+alpha)/6.0)*8));
        /*g.drawImage(Art.logo, 0, yo, null);*/
        g.drawBitmap(Art.logo, 0, 0, null);
        g.drawBitmap(Art.titleScreen, 0, 120, null);
        
        drawStringDropShadow(g,"Enter Name:" ,5,11,1);
        drawStringDropShadow(g,name ,16,11,1);
        /*
        drawStringDropShadow(g,"1. Use the 'S' key to:" ,2,9,1);
        drawStringDropShadow(g,"Start game and Start a challenge" ,5,10,2);
        drawStringDropShadow(g,"Jump" ,5,11,3);
        drawStringDropShadow(g,"2. Use the arrow keys to:" ,2,12,4);
        drawStringDropShadow(g,"Move Mario to a challenge" ,5,13,5);
        drawStringDropShadow(g,"Run forward or back." ,5,14,6);
        drawStringDropShadow(g,"3. Use the 'Q' key to eat" ,2,15,7);
        drawStringDropShadow(g,"or take insulin" ,5,16,2);
        //drawStringDropShadow(g,"by jumping over them or squishing them by jumping on top of them." ,5,13,3);


*/
       // drawStringDropShadow(g,"'s' key to play world" ,7,11,3);
       // drawStringDropShadow(g,"'s' key play..." ,13,12,3);
    }
    private void drawStringDropShadow(Canvas g, String text, int x, int y,
			int c) {
		drawString(g, text, x * 8 + 5, y * 8 + 5, 0);
		drawString(g, text, x * 8 + 4, y * 8 + 4, c);
	}

	private void drawString(Canvas g, String text, int x, int y, int c) {
		char[] ch = text.toCharArray();
		for (int i = 0; i < ch.length; i++) {
			g.drawBitmap(Art.font[ch[i] - 32][c], x + i * 8, y, null);
		}
	}
    /*private void drawString(Graphics g, String text, int x, int y, int c)
    {
        char[] ch = text.toCharArray();
        for (int i = 0; i < ch.length; i++)
        {
            g.drawImage(Art.font[ch[i] - 32][c], x + i * 8, y, null);
        }
    }*/

	public void sendKeyChar(char character)
    {
		switch (character)
		{
		case '\b':
			if (name.length() !=0)
			name = name.substring(0, name.length()-1);
			
			break;
		case '\t':
		case '\n':
		case '\\':
		case '\r':
		case '\f':
		case '\'':
		case '\"':
		case ' ':
		break;
		default:
			 name += character;
			break;
		}
    }
    private boolean wasDown = true;
    public void tick()
    {
        tick++;
        //if (!name.equals(""))
        //{
        if (!wasDown && keys[Mario.KEY_ENTER])
        {
        	sessionNum++;
        	CSVLogger.LogStart(name, sessionNum);
            component.startGame();
        }
        if (keys[Mario.KEY_ENTER])
        {
            wasDown = false;
        }
        //}
    }

    public float getX(float alpha)
    {
        return 0;
    }

    public float getY(float alpha)
    {
        return 0;
    }

}
