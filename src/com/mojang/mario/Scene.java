package com.mojang.mario;

import android.graphics.Canvas;
import android.graphics.Paint;

import com.mojang.sonar.AndroidSoundEngine;
import com.mojang.sonar.SonarSoundEngine;
import com.mojang.sonar.SoundListener;


public abstract class Scene implements SoundListener
{
   
    public static String name = "";
    public static String filePath = "";
    public static int sessionNum = 0;
    public static boolean[] keys = new boolean[16];

    public void toggleKey(int key, boolean isPressed)
    {
        keys[key] = isPressed;
    }

    public void sendKeyChar(char character)
    {
    	
    }
   

    public abstract void init();

    public abstract void tick();

    public abstract void render(Canvas og, float alpha, Paint paint);
}