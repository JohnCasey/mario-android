package com.mojang.mario.sprites;

//import java.awt.Graphics;
//import java.awt.Image;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;

import com.mojang.mario.level.SpriteTemplate;
import com.mojang.sonar.SoundSource;

public class Sprite implements SoundSource
{
    public static SpriteContext spriteContext;
    
    public float xOld, yOld, x, y, xa, ya;
    
    public int xPic, yPic;
    public int wPic = 32;
    public int hPic = 32;
    public int xPicO, yPicO;
    public boolean xFlipPic = false;
    public boolean yFlipPic = false;
    public Bitmap[][] sheet;
    public boolean visible = true;
    
    public int layer = 1;

    public SpriteTemplate spriteTemplate;
    
    public void move()
    {
        x+=xa;
        y+=ya;
    }
    
    public void render(Canvas og, float alpha)
    {
        if (!visible) return;
        
        int xPixel = (int)(xOld+(x-xOld)*alpha)-xPicO;
        int yPixel = (int)(yOld+(y-yOld)*alpha)-yPicO;

       // sheet[xPic][yPic].
        
        if (xFlipPic || yFlipPic)
        {
        	Matrix flipHorizontalMatrix = new Matrix();
        	float xScale = 1;
        	float xPos = xPixel;
        	float yScale = 1;
        	float yPos = yPixel;
        	if (xFlipPic)
        	{
        		xScale= -1;
        		xPos +=  sheet[xPic][yPic].getWidth();
        	}
        	if (yFlipPic)
        	{
        		//yScale= -1;
        		yPos +=  sheet[xPic][yPic].getHeight();
        	}
        	flipHorizontalMatrix.setScale(xScale,yScale);
        	flipHorizontalMatrix.postTranslate(xPos,yPos);
        	 og.drawBitmap(sheet[xPic][yPic],flipHorizontalMatrix, null);

        }
        else
        	og.drawBitmap(sheet[xPic][yPic],null, new Rect(  xPixel, yPixel,wPic+xPixel, hPic+yPixel), null);

        //og.drawBitmap(sheet[xPic][yPic],null, new Rect(  xPixel+(xFlipPic?wPic:0), yPixel+(yFlipPic?hPic:0), xFlipPic?-wPic+ xPixel:wPic+xPixel, yFlipPic?-hPic+yPixel:hPic+yPixel), null);
        
        // og.drawBitmap(sheet[0][0],null, new Rect(  100,100, 100 + sheet[0][0].getWidth(), 100 + sheet[0][0].getHeight()), null);
    }
    
/*  private void blit(Graphics og, Image bitmap, int x0, int y0, int x1, int y1, int w, int h)
    {
        if (!xFlipPic)
        {
            if (!yFlipPic)
            {
                og.drawImage(bitmap, x0, y0, x0+w, y0+h, x1, y1, x1+w, y1+h, null);
            }
            else
            {
                og.drawImage(bitmap, x0, y0, x0+w, y0+h, x1, y1+h, x1+w, y1, null);
            }
        }
        else
        {
            if (!yFlipPic)
            {
                og.drawImage(bitmap, x0, y0, x0+w, y0+h, x1+w, y1, x1, y1+h, null);
            }
            else
            {
                og.drawImage(bitmap, x0, y0, x0+w, y0+h, x1+w, y1+h, x1, y1, null);
            }
        }
    }*/

    public final void tick()
    {
        xOld = x;
        yOld = y;
        move();
    }

    public final void tickNoMove()
    {
        xOld = x;
        yOld = y;
    }

    public float getX(float alpha)
    {
        return (xOld+(x-xOld)*alpha)-xPicO;
    }

    public float getY(float alpha)
    {
        return (yOld+(y-yOld)*alpha)-yPicO;
    }

    public void collideCheck()
    {
    }

    public void bumpCheck(int xTile, int yTile)
    {
    }

    public boolean shellCollideCheck(Shell shell)
    {
        return false;
    }

    public void release(Mario mario)
    {
    }

    public boolean fireballCollideCheck(Fireball fireball)
    {
        return false;
    }
}